import React from 'react';
import { Link } from 'react-router-dom';
import { CiLocationOn } from "react-icons/ci"
import { IoCallOutline } from 'react-icons/io5'
import { FiMail } from 'react-icons/fi'
import { BsFacebook, BsMessenger, BsWhatsapp, BsYoutube } from 'react-icons/bs';
import logo from '../../assets/logo/logo.jpeg'

const Footer = () => {
    return (
        <footer className='bg-[#f1fdff] print:hidden'>
            <div className='container mx-auto '>
                <div className='grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 mx-4  border-b-2 border-gray-600 py-4'>
                    <div>
                        <img className='w-36' src={logo} alt="logo" />
                        <p className='my-2'>আমাদের মোবাইল অ্যাপ ডাউনলোড করুন</p>
                        <img src="https://upload.wikimedia.org/wikipedia/commons/7/78/Google_Play_Store_badge_EN.svg" alt="" />
                    </div>
                    <div className='my-3 md:my-0'>
                        <h3 className='text-2xl font-bold mb-2 '>প্রতিষ্ঠান</h3>
                        <div className='flex flex-col justify-start gap-1 text-gray-700'>
                            <Link className='hover:text-blue-700' to="/aboutme">আমাদের সম্পর্কে</Link>
                            <Link className='hover:text-blue-700' to="/contact">যোগাযোগ</Link>
                            <Link className='hover:text-blue-700' to="/notice">নোটিশ</Link>
                        </div>
                    </div>
                    <div>
                        <h3 className='text-2xl font-bold mb-2 '>লিঙ্ক</h3>
                        <div className='flex flex-col justify-start gap-1 text-gray-700'>
                            <Link className='hover:text-blue-700' to="/policy">নীতিমালা</Link>
                            <Link className='hover:text-blue-700' to="/privacypolicy">প্রাইভেসি পলিসি</Link>
                            <Link className='hover:text-blue-700' to="/refundpolicy">রিফান্ড নীতিমালা</Link>
                        </div>
                    </div>
                    <div className='my-3 md:my-0'>
                        <h3 className='text-2xl font-bold mb-2'>যোগাযোগ</h3>
                        <div className='flex flex-col justify-start'>
                            <div className='flex justify-start items-center gap-2'>
                                <CiLocationOn />
                                <address>কাটলী, নেত্রকোনা সদর, নেত্রকোনা</address>
                            </div>
                            <div className='flex justify-start items-start gap-2'>
                                <IoCallOutline />
                                <div>
                                    <p>01717-355117</p>
                                </div>
                            </div>
                            <div className='flex justify-start items-start gap-2'>
                                <FiMail />
                                <p>unmilan2011@gmail.com</p>
                            </div>
                            <div className='flex justify-start items-center gap-2 text-3xl text-white font-bold p-3 mx-auto md:mx-0'>
                                <a href="#"><BsFacebook className='bg-[#15ade2] p-2 rounded-full hover:scale-105' /></a>
                                <a href="#"><BsMessenger className='bg-[#15ade2] p-2 rounded-full hover:scale-105' /></a>
                                <a href="#"><BsWhatsapp className='bg-[#01e7a2] p-2 rounded-full hover:scale-105' /></a>
                                <a href="#"><BsYoutube className='bg-[#ff0049] p-2 rounded-full hover:scale-105' /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='text-center py-5 text-gray-700 px-5'>
                    <h3>এই ওয়েবসাইটের সকল ধরনের লেখা, ছবি, অডিও এবং ভিডিও অনুমতি ছাড়া ব্যবহার সম্পূর্ণ বেআইনী।</h3>
                    <p className='my-3'>স্বত্ব © Unmilon Studio কর্তৃক সর্বস্বত্ব সংরক্ষিত | ডেভেলপার <a href="https://xozosoft.com/">Xozosoft Solution</a></p>
                </div>
            </div>
        </footer>
    );
};

export default Footer;