import React from 'react';
import { Link } from 'react-router-dom';
import auth from '../../assets/auth/auth.png'

const ForgotPassword = () => {
    return (
        <div>
            <div className='my-10 container mx-auto'>
                <div className='flex justify-between items-center w-full lg:w-2/3 gap-5 mx-auto bg-slate-200 rounded-lg p-10'>
                    <div className='hidden lg:block'><img src={auth} alt="" /></div>
                    <div className='w-96'>
                        <div>
                            <h2 className='text-center my-3 text-4xl font-bold text-black'>Forgot Password</h2>
                        </div>
                        <form>
                            <div className='flex flex-col'>
                                <label className="custom-field one">
                                    <input type="email" placeholder=" " required />
                                    <span className="placeholder">Enter Email</span>
                                </label>
                                <input className='my-5 w-full bg-[#d1a522] p-2 rounded-2xl text-xl font-bold text-white hover:bg-[#ad8819] duration-200 cursor-pointer' type="submit" value="Request Password Reset" />
                            </div>
                        </form>
                        <Link to="/login" className='flex justify-center text-xl font-bold underline text-blue-500 my-3'>Login</Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ForgotPassword;