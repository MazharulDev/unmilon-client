
import React from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { AiOutlineUser } from 'react-icons/ai';
import { FaEdit } from 'react-icons/fa';
import { useQuery } from 'react-query';
import { Link, Outlet } from 'react-router-dom';
import auth from '../../firebase.init';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';



const UserProfile = () => {
    const [user, loading] = useAuthState(auth);
    const { data: userInfo, isLoading, refetch } = useQuery('userInfo', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/userprofile?email=${user?.email}`, {
        method: 'GET',
    })
        .then(res => res.json()))
    if (loading || isLoading) {
        return <LoadingSpinner />
    }
    refetch();
    // const photo = user?.reloadUserInfo?.photoUrl;
    const info = userInfo?.data[0]
    if (loading) {
        return <LoadingSpinner />
    }
    return (
        <div>
            <div className='container mx-auto'>
                <div className="drawer drawer-mobile my-5">
                    <input id="profile-drawer" type="checkbox" className="drawer-toggle" />
                    <div className="drawer-content  bg-green-100 rounded-2xl mx-5">
                        {/* <!-- Page content here --> */}
                        <h2 className='my-5 text-3xl mx-5'>Hey, <span className='font-bold text-yellow-500'>{userInfo?.data[0]?.name}</span></h2>


                        <Outlet />
                        <label htmlFor="profile-drawer" className="btn btn-primary drawer-button lg:hidden">Open drawer</label>

                    </div>
                    <div className="drawer-side">
                        <label htmlFor="profile-drawer" className="drawer-overlay"></label>
                        <ul className="menu p-4 w-52 bg-base-200 text-base-content rounded-xl">
                            {/* <!-- Sidebar content here --> */}
                            <div className="avatar flex justify-center my-5">
                                <div className="w-14 rounded-full ring ring-primary ring-offset-base-100 ring-offset-2">
                                    {info?.imgURL ? <img src={info?.imgURL} alt='' /> : <div className='text-5xl h-full text-white bg-blue-500 flex justify-center items-center font-bold'>{info?.name.substring(0, 1)}</div>}
                                </div>
                            </div>
                            <div className=''>
                                <div className=''>
                                    <li>
                                        <div className='flex justify-start items-center text-xl font-bold'>
                                            <AiOutlineUser /> <Link to="/profile">My Profile</Link>
                                        </div>
                                    </li>
                                    <li>
                                        <div className='flex justify-start items-center text-xl font-bold'>
                                            <FaEdit /> <Link to="/profile/edit">Edit Profile</Link>
                                        </div>
                                    </li>

                                </div>
                            </div>
                        </ul>

                    </div>
                </div>

            </div>
            {/* <h2 className='my-5 text-3xl mx-10'>Hi, {user.displayName}</h2>
                <h2 className='text-center text-4xl border-b-2 border-blue-400 font-bold mx-10 mb-5'>Profile</h2>
                 */}
        </div>
    );
};


export default UserProfile;


