import React, { useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useForm } from 'react-hook-form';
import { useQuery } from 'react-query';
import auth from '../../firebase.init';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';
import { toast } from 'react-toastify'

const EditUserProfile = () => {
    const [profileImg, setProfileImg] = useState(null)
    // console.log(imgUrl);
    const handlePhotoUpload = () => {
        const formData = new FormData();
        formData.append('userImg', profileImg);
        const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/users/profile-img`;
        fetch(url, {
            method: "POST",
            body: formData
        })
            .then(res => res.json())
            .then(output => {
                if (output.error) {
                    toast.error(output.error);
                } else {
                    const pic = {
                        imgURL: output.imgURL
                    }
                    fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/updateuser-photo/${user?.email}`, {
                        method: 'PUT',
                        headers: {
                            'content-type': 'application/json'
                        },
                        body: JSON.stringify(pic)
                    })
                        .then(res => res.json())
                        .then(result => {
                            if (result?.data?.modifiedCount > 0) {
                                toast.success("Profile photo update successfully")
                            }
                        })
                }
            })
    }
    const [user, loading] = useAuthState(auth);
    const { register, formState: { errors }, handleSubmit } = useForm();
    const onSubmit = async data => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/updateuser/${user?.email}`, {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                if (result?.data?.modifiedCount > 0) {
                    toast.success("Profile update successfully")
                }
            })
    }
    const { data: userInfo, isLoading, refetch } = useQuery('userInfo', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/userprofile?email=${user?.email}`, {
        method: 'GET',
    })
        .then(res => res.json()))
    if (loading || isLoading) {
        return <LoadingSpinner />
    }
    refetch();
    // const photo = user.reloadUserInfo.photoUrl;
    const info = userInfo?.data[0];

    return (
        <div>
            <div className='flex justify-start'>
                <div className='mx-10 my-5'>
                    <div className="avatar flex justify-start ml-3 mb-5">
                        <div className="w-24 rounded-full ring ring-primary ring-offset-base-100 ring-offset-2">
                            {info?.imgURL ? <img src={info?.imgURL} alt='' /> : <div className='text-7xl h-full text-white bg-blue-500 flex justify-center items-center font-bold'>{info?.name.substring(0, 1)}</div>}
                        </div>
                    </div>
                    <input onChange={(e) => setProfileImg(e.target.files[0])} type="file" className='file-input file-input-bordered file-input-xs w-28 max-w-xs' /> <br />
                    <button className='btn btn-xs btn-primary ml-6' onClick={handlePhotoUpload}>Upload</button>
                </div>
                <form onSubmit={handleSubmit(onSubmit)} className='ml-5 mt-2 grid lg:grid-cols-3 gap-10'>
                    <div>
                        <div>
                            <h2 className='text-lg font-bold'>Full Name:</h2>
                            <input type="text" placeholder="Type your full name" className="input input-bordered input-primary input-sm w-full max-w-xs" defaultValue={info?.name}
                                {...register("name",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Name is Required'
                                        },

                                        minLength: {
                                            value: 3,
                                            message: 'Must 3 character Name'
                                        }
                                    }
                                )}
                            />
                        </div>
                        <label className="label">
                            {errors.name?.type === 'required' && <span className="label-text-alt text-red-500">{errors.name.message}</span>}
                            {errors.name?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.name.message}</span>}
                        </label>
                        <div>
                            <h2 className='text-lg font-bold'>Email Address:</h2>
                            <input type="text" placeholder="Type your email address" className="input input-bordered input-primary input-sm w-full max-w-xs" value={user?.email}
                                {...register("email",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Email is Required'
                                        },

                                        pattern: {
                                            value: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/,
                                            message: 'Provide a Valid Email'
                                        }
                                    }
                                )}
                            />
                        </div>
                        <label>
                            <p className='text-xs text-gray-500'>Email address can't be changed</p>
                            {errors.email?.type === 'required' && <span className="label-text-alt text-red-500">{errors.email.message}</span>}
                            {errors.email?.type === 'pattern' && <span className="label-text-alt text-red-500">{errors.email.message}</span>}
                        </label>
                        <div>
                            <h2 className='text-lg font-bold'>Phone Number:</h2>
                            <input type="text" placeholder="Type your phone number" className="input input-bordered input-primary input-sm w-full max-w-xs" defaultValue={info?.phoneNumber}
                                {...register("phoneNumber",
                                    {
                                        required: {
                                            value: true,
                                            message: 'phone number is required'
                                        },

                                        minLength: {
                                            value: 10,
                                            message: 'Must 10 character phone number'
                                        }
                                    }
                                )}
                            />
                        </div>
                        <label className="label">
                            {errors.phoneNumber?.type === 'required' && <span className="label-text-alt text-red-500">{errors.phoneNumber.message}</span>}
                            {errors.phoneNumber?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.phoneNumber.message}</span>}
                        </label>
                        <div>
                            <h2 className='text-lg font-bold'>Institute:</h2>
                            <input type="text" placeholder="Type your Institute" className="input input-bordered input-primary input-sm w-full max-w-xs" defaultValue={info?.institute}
                                {...register("institute",
                                    {
                                        required: {
                                            value: true,
                                            message: 'institute is Required'
                                        },
                                        minLength: {
                                            value: 5,
                                            message: 'Must 5 character institute name'
                                        }
                                    }
                                )}
                            />
                        </div>
                        <label className="label">
                            {errors.institute?.type === 'required' && <span className="label-text-alt text-red-500">{errors.institute.message}</span>}
                            {errors.institute?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.institute.message}</span>}
                        </label>
                    </div>
                    <div>
                        <div>
                            <h2 className='text-lg font-bold'>Your gender:</h2>
                            <select className="select select-bordered border-primary select-sm w-full max-w-xs"
                                {...register("gender",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Gender select is Required'
                                        }
                                    }
                                )}
                            >
                                <option disabled selected>Your gender</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                                <option value="third-gender">Third Gender</option>
                            </select>
                        </div>
                        <label className="label">
                            {errors.gender?.type === 'required' && <span className="label-text-alt text-red-500">{errors.gender.message}</span>}
                        </label>
                        <div>
                            <h2 className='text-lg font-bold'>Your religion:</h2>
                            <select className="select select-bordered border-primary select-sm w-full max-w-xs"

                                {...register("religion",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Religion select is Required'
                                        }
                                    }
                                )}
                            >
                                <option disabled selected>Your religion</option>
                                <option value="islam">Islam</option>
                                <option value="hindu">Hindu</option>
                                <option value="others">Others</option>
                            </select>
                        </div>
                        <label className="label">
                            {errors.religion?.type === 'required' && <span className="label-text-alt text-red-500">{errors.religion.message}</span>}
                        </label>
                        <div>
                            <h2 className='text-lg font-bold'>Your Nationality:</h2>
                            <input type="text" placeholder="Type your nationality" className="input input-bordered input-primary input-sm w-full max-w-xs" defaultValue={info?.nationality}
                                {...register("nationality",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Nationality is Required'
                                        }
                                    }
                                )}
                            />
                        </div>
                        <label className="label">
                            {errors.nationality?.type === 'required' && <span className="label-text-alt text-red-500">{errors.nationality.message}</span>}
                        </label>
                        <div>
                            <h2 className='text-lg font-bold'>Your Postcode:</h2>
                            <input type="number" placeholder="Type your postcode" className="input input-bordered input-primary input-sm w-full max-w-xs" defaultValue={info?.postcode}
                                {...register("postcode",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Postcode is Required'
                                        }
                                    }
                                )}
                            />
                        </div>
                        <label className="label">
                            {errors.postcode?.type === 'required' && <span className="label-text-alt text-red-500">{errors.postcode.message}</span>}
                        </label>
                    </div>
                    <div>
                        <div>
                            <h2 className='text-lg font-bold'>Your Date of birth:</h2>
                            <input type="date" className='input input-bordered input-primary input-sm w-full max-w-xs'
                                defaultValue={info?.dateofbirth}
                                {...register("dateofbirth",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Date of birth is Required'
                                        }
                                    }
                                )}
                            />
                        </div>
                        <label className="label">
                            {errors.dateofbirth?.type === 'required' && <span className="label-text-alt text-red-500">{errors.dateofbirth.message}</span>}
                        </label>
                        <div>
                            <h2 className='text-lg font-bold'>Present Address:</h2>
                            <textarea className="textarea textarea-primary" placeholder="Type present address"
                                defaultValue={info?.presentaddress}
                                {...register("presentaddress",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Present Address is Required'
                                        }
                                    }
                                )}
                            ></textarea>
                        </div>
                        <label className="label">
                            {errors.presentaddress?.type === 'required' && <span className="label-text-alt text-red-500">{errors.presentaddress.message}</span>}
                        </label>
                        <div>
                            <h2 className='text-lg font-bold'>Permanent Address:</h2>
                            <textarea className="textarea textarea-primary" placeholder="Type permanent address"
                                defaultValue={info?.permanentaddress}
                                {...register("permanentaddress",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Permanent Address is Required'
                                        }
                                    }
                                )}
                            ></textarea>
                        </div>
                        <label className="label">
                            {errors.permanentaddress?.type === 'required' && <span className="label-text-alt text-red-500">{errors.permanentaddress.message}</span>}
                        </label>
                        <div className='flex justify-end mt-4'>
                            <input type="submit" className='btn btn-success btn-sm' value="Save" />
                        </div>
                    </div>

                </form>

            </div>

        </div>
    );
};

export default EditUserProfile;