import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { BsFillEyeFill, BsFillEyeSlashFill } from 'react-icons/bs';
import { Link, useNavigate } from 'react-router-dom';
import authImg from '../../assets/auth/auth.png'
import './auth.css'
import { useCreateUserWithEmailAndPassword, useUpdateProfile } from 'react-firebase-hooks/auth';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';
import auth from '../../firebase.init';
import { toast } from 'react-toastify';

const SignUp = () => {
    const navigate = useNavigate()
    const [error, setError] = useState('')
    const [
        createUserWithEmailAndPassword,
        userWithEmail,
        loadingWithEmail,
        errorWithEmail,
    ] = useCreateUserWithEmailAndPassword(auth, { sendEmailVerification: true });
    const [updateProfile, updating] = useUpdateProfile(auth);
    const { register, formState: { errors }, handleSubmit } = useForm();
    const onSubmit = async data => {
        if (data.password !== data.confirmPassword) {
            setError('password & confrim password did not match')
            return;
        }
        await createUserWithEmailAndPassword(data.email, data.password)
        await updateProfile({ displayName: data.name })

        const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/users`
        fetch(url, {
            method: "POST",
            headers: {
                "content-type": 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(result => {
                // localStorage.setItem('accessToken', result.accessToken);
                // navigate(from, { replace: true });
                if (result) {
                    navigate('/')
                }
            })


    }
    if (userWithEmail) {
        toast("Create Account successfully")
    }


    const [open, setOpen] = useState(false)

    // handle toggle 
    const toggle = () => {
        setOpen(!open)
    }
    if (loadingWithEmail || updating) {
        return <LoadingSpinner />
    }
    return (
        <div>
            <div className='my-10 container mx-auto'>
                <div className='flex justify-between items-center w-full lg:w-2/3 gap-5 mx-auto bg-slate-200 rounded-lg p-10'>
                    <div className='hidden lg:block'><img src={authImg} alt="" /></div>
                    <div className='w-96'>
                        <div>
                            <h2 className='text-center my-3 text-4xl font-bold text-black'>Register</h2>
                        </div>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className='flex flex-col'>
                                <label className="custom-field one">
                                    <input type="text" placeholder=" "
                                        {...register("name",
                                            {
                                                required: {
                                                    value: true,
                                                    message: 'Name is Required'
                                                },

                                                minLength: {
                                                    value: 3,
                                                    message: 'Must 3 character Name'
                                                }
                                            }
                                        )}
                                    />
                                    <span className="placeholder">Name (In English)</span>
                                </label>
                                <label className="label">
                                    {errors.name?.type === 'required' && <span className="label-text-alt text-red-500">{errors.name.message}</span>}
                                    {errors.name?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.name.message}</span>}
                                </label>
                                <label className="custom-field one">
                                    <input type="email" placeholder=" "
                                        {...register("email",
                                            {
                                                required: {
                                                    value: true,
                                                    message: 'Email is Required'
                                                },

                                                pattern: {
                                                    value: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/,
                                                    message: 'Provide a Valid Email'
                                                }
                                            }
                                        )}
                                    />
                                    <span className="placeholder">Email</span>

                                </label>
                                <label className="label">
                                    {errors.email?.type === 'required' && <span className="label-text-alt text-red-500">{errors.email.message}</span>}
                                    {errors.email?.type === 'pattern' && <span className="label-text-alt text-red-500">{errors.email.message}</span>}
                                </label>
                                <label className="custom-field one">
                                    <input type="text" placeholder=" "
                                        {...register("institute",
                                            {
                                                required: {
                                                    value: true,
                                                    message: 'institute is Required'
                                                },
                                                minLength: {
                                                    value: 5,
                                                    message: 'Must 5 character institute name'
                                                }
                                            }
                                        )}
                                    />
                                    <span className="placeholder">Institute</span>

                                </label>
                                <label className="label">
                                    {errors.institute?.type === 'required' && <span className="label-text-alt text-red-500">{errors.institute.message}</span>}
                                    {errors.institute?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.institute.message}</span>}
                                </label>
                                <label className="custom-field one">
                                    <input type="number" placeholder=" "
                                        {...register("phoneNumber",
                                            {
                                                required: {
                                                    value: true,
                                                    message: 'phone number is required'
                                                },

                                                minLength: {
                                                    value: 10,
                                                    message: 'Must 10 character phone number'
                                                }
                                            }
                                        )}
                                    />
                                    <span className="placeholder">Phone</span>
                                </label>
                                <label className="label">
                                    {errors.phoneNumber?.type === 'required' && <span className="label-text-alt text-red-500">{errors.phoneNumber.message}</span>}
                                    {errors.phoneNumber?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.phoneNumber.message}</span>}
                                </label>
                                <label className="custom-field one">
                                    <div className='flex justify-end items-center'>
                                        <input type={(open === false) ? 'password' : 'text'} placeholder=" "
                                            {...register("password",
                                                {
                                                    required: {
                                                        value: true,
                                                        message: 'Password is Required'
                                                    },

                                                    minLength: {
                                                        value: 6,
                                                        message: 'Must 6 character in  Password'
                                                    }
                                                }
                                            )}
                                        />
                                        <span className="placeholder">Password</span>
                                        {
                                            (open === false) ? <BsFillEyeSlashFill className='absolute mr-3 text-xl cursor-pointer' onClick={toggle} /> :
                                                <BsFillEyeFill className='absolute mr-3 text-xl cursor-pointer' onClick={toggle} />


                                        }
                                    </div>
                                </label>
                                <label className="label">
                                    {errors.password?.type === 'required' && <span className="label-text-alt text-red-500">{errors.password.message}</span>}
                                    {errors.password?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.password.message}</span>}
                                </label>
                                <label className="custom-field one">
                                    <div className='flex justify-end items-center'>
                                        <input type={(open === false) ? 'password' : 'text'} placeholder=" "
                                            {...register("confirmPassword",
                                                {
                                                    required: {
                                                        value: true,
                                                        message: 'Password is Required'
                                                    },

                                                    minLength: {
                                                        value: 6,
                                                        message: 'Must 6 character in  Password'
                                                    }
                                                }
                                            )}
                                        />
                                        <span className="placeholder">Confirm_password</span>
                                        {
                                            (open === false) ? <BsFillEyeSlashFill className='absolute mr-3 text-xl cursor-pointer' onClick={toggle} /> :
                                                <BsFillEyeFill className='absolute mr-3 text-xl cursor-pointer' onClick={toggle} />


                                        }
                                    </div>
                                </label>
                                <label className="label">
                                    {errors.confirmPassword?.type === 'required' && <span className="label-text-alt text-red-500">{errors.confirmPassword.message}</span>}
                                    {errors.confirmPassword?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.confirmPassword.message}</span>}
                                </label>
                                <p className='text-red-500 my-4'>{error}</p>
                                <input className='my-5 w-full bg-[#d1a522] p-2 rounded-2xl text-xl font-bold text-white hover:bg-[#ad8819] duration-200 cursor-pointer' type="submit" value="Register" />
                            </div>
                            <div>
                                <p className='my-1 text-gray-700 text-right'>Already have an account? <Link className='text-blue-500 underline' to="/login">Login</Link></p>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SignUp;