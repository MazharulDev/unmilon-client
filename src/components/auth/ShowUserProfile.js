import React from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useQuery } from 'react-query';
import auth from '../../firebase.init';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';
import { Link } from 'react-router-dom'

const ShowUserProfile = () => {
    const [user, loading] = useAuthState(auth);
    const { data: userInfo, isLoading, refetch } = useQuery('userInfo', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/userprofile?email=${user?.email}`, {
        method: 'GET',
    })
        .then(res => res.json()))
    if (loading || isLoading) {
        return <LoadingSpinner />
    }
    refetch();
    // const photo = user.reloadUserInfo.photoUrl;
    const info = userInfo?.data[0];
    return (
        <div className='flex justify-start'>
            <div className='mx-10 my-5'>
                <div className="avatar flex justify-start ml-3 mb-5">
                    <div className="w-24 rounded-full ring ring-primary ring-offset-base-100 ring-offset-2">
                        {info?.imgURL ? <img src={info?.imgURL} alt='' /> : <div className='text-7xl h-full text-white bg-blue-500 flex justify-center items-center font-bold'>{info?.name.substring(0, 1)}</div>}
                    </div>
                </div>
                <Link to="/profile/edit" className='btn btn-sm btn-primary'>Edit Profile</Link>
            </div>
            <div className='ml-5 mt-2 grid lg:grid-cols-3 gap-10'>
                <div>
                    <div className='my-3'>
                        <h2 className='text-lg font-bold'>Full Name:</h2>
                        <p>{info?.name}</p>
                    </div>
                    <div className='my-3'>
                        <h2 className='text-lg font-bold'>Email Adress:</h2>
                        <p>{user?.email}</p>
                    </div>
                    <div className='my-3'>
                        <h2 className='text-lg font-bold'>Phone Number:</h2>
                        <p>{info?.phoneNumber}</p>
                    </div>
                    <div className='my-3'>
                        <h2 className='text-lg font-bold'>Institute:</h2>
                        <p>{info?.institute}</p>
                    </div>
                </div>
                <div>
                    {
                        info?.gender && <div className='my-3'>
                            <h2 className='text-lg font-bold'>Gender:</h2>
                            <p>{info?.gender}</p>
                        </div>
                    }
                    {
                        info?.religion && <div className='my-3'>
                            <h2 className='text-lg font-bold'>Religion:</h2>
                            <p>{info?.religion}</p>
                        </div>
                    }
                    {
                        info?.nationality && <div className='my-3'>
                            <h2 className='text-lg font-bold'>Nationality:</h2>
                            <p>{info?.nationality}</p>
                        </div>
                    }
                    {
                        info?.postcode && <div className='my-3'>
                            <h2 className='text-lg font-bold'>Post code:</h2>
                            <p>{info?.postcode}</p>
                        </div>
                    }
                </div>
                <div>
                    {
                        info?.dateofbirth && <div className='my-3'>
                            <h2 className='text-lg font-bold'>Date of Birth:</h2>
                            <p>{info?.dateofbirth}</p>
                        </div>
                    }
                    {
                        info?.presentaddress && <div className='my-3'>
                            <h2 className='text-lg font-bold'>Present Address:</h2>
                            <p>{info?.presentaddress}</p>
                        </div>
                    }
                    {
                        info?.permanentaddress && <div className='my-3'>
                            <h2 className='text-lg font-bold'>Permanent Address:</h2>
                            <p>{info?.permanentaddress}</p>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
};

export default ShowUserProfile;