import React, { useState } from 'react';
import { useSignInWithEmailAndPassword } from 'react-firebase-hooks/auth';
import { useForm } from 'react-hook-form';
import { BsFillEyeFill, BsFillEyeSlashFill } from 'react-icons/bs';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import authImg from '../../assets/auth/auth.png'
import auth from '../../firebase.init';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';
import './auth.css'

const Login = () => {
    const navigate = useNavigate()
    const [
        signInWithEmailAndPassword,
        user,
        loading,
        error,
    ] = useSignInWithEmailAndPassword(auth);
    const { register, formState: { errors }, handleSubmit } = useForm();
    const onSubmit = async data => {
        await signInWithEmailAndPassword(data.email, data.password)
    }
    const [open, setOpen] = useState(false)

    // handle toggle 
    const toggle = () => {
        setOpen(!open)
    }
    if (error) {
        toast.error(error.message)
    }
    if (user) {
        toast("Login successfull")
        navigate('/')
    }
    if (loading) {
        return <LoadingSpinner />
    }
    return (
        <div>
            <div className='my-10 container mx-auto'>
                <div className='flex justify-between items-center w-full lg:w-2/3 gap-5 mx-auto bg-slate-200 rounded-lg p-10'>
                    <div className='hidden lg:block'><img src={authImg} alt="" /></div>
                    <div className='w-96'>
                        <div>
                            <h2 className='text-center my-3 text-4xl font-bold text-black'>Login</h2>
                        </div>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className='flex flex-col'>
                                <label className="custom-field one">
                                    <input type="text" placeholder=" "
                                        {...register("email",
                                            {
                                                required: {
                                                    value: true,
                                                    message: 'Email is Required'
                                                },

                                                pattern: {
                                                    value: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/,
                                                    message: 'Provide a Valid Email'
                                                }
                                            }
                                        )}
                                    />
                                    <span className="placeholder">Email</span>
                                </label>
                                <label className="label">
                                    {errors.email?.type === 'required' && <span className="label-text-alt text-red-500">{errors.email.message}</span>}
                                    {errors.email?.type === 'pattern' && <span className="label-text-alt text-red-500">{errors.email.message}</span>}
                                </label>
                                <label className="custom-field one">
                                    <div className='flex justify-end items-center'>
                                        <input type={(open === false) ? 'password' : 'text'} placeholder=" "
                                            {...register("password",
                                                {
                                                    required: {
                                                        value: true,
                                                        message: 'Password is Required'
                                                    },

                                                    minLength: {
                                                        value: 6,
                                                        message: 'Must 6 character in  Password'
                                                    }
                                                }
                                            )}
                                        />
                                        <span className="placeholder">Password</span>
                                        {
                                            (open === false) ? <BsFillEyeSlashFill className='absolute mr-3 text-xl cursor-pointer' onClick={toggle} /> :
                                                <BsFillEyeFill className='absolute mr-3 text-xl cursor-pointer' onClick={toggle} />


                                        }
                                    </div>
                                </label>
                                <label className="label">
                                    {errors.password?.type === 'required' && <span className="label-text-alt text-red-500">{errors.password.message}</span>}
                                    {errors.password?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.password.message}</span>}
                                </label>
                                <input className='my-5 w-full bg-[#d1a522] p-2 rounded-2xl text-xl font-bold text-white hover:bg-[#ad8819] duration-200 cursor-pointer' type="submit" value="Login" />
                            </div>
                            <div className='flex justify-end'>
                                <Link to="/forgot-password" className='my-2 text-red-500 underline hover:text-red-500'>Forgot Password?</Link>
                            </div>
                            <div>
                                <p className='text-center my-3'>Don't have an account? <Link className='text-blue-500 underline' to="/signup">register</Link></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;