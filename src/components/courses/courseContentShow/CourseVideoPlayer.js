import React from 'react';
import ReactPlayer from 'react-player';
// import getYouTubeID from 'get-youtube-id';


const CourseVideoPlayer = ({ courseContent, index, handlePrev, handleNext }) => {
    // const url = singleVideo ? singleVideo?.link : courseContent[0]?.link
    // const id = getYouTubeID(url)
    return (
        <div className='h-[90vh]'>
            {/* class pointer-events-none */}
            <div className='h-[70%]'>
                <ReactPlayer
                    controls={true}
                    config={{
                        file: {
                            attributes: {
                                controlsList: 'nodownload'

                            }
                        },
                        youtube: {
                            playerVars: { showinfo: 0 }
                        },
                    }}
                    width="100%"
                    height="100%"
                    onSeek={true}
                    playing={true}
                    // url={singleVideo ? singleVideo?.link : courseContent[0]?.link}
                    url={courseContent?.link}
                    // url={"http://localhost:5000/course-video/1675587815446-391766755-Drag%20and%20Drop%20CSV%20Files%20-%20React%20Js.mp4"}
                    // url={`https://www.youtube.com/embed/${id}?rel=0`}
                    light={true}
                />

            </div>
            <div className='flex justify-between'>
                <div className='mt-5'>
                    {/* <h2 className='text-3xl font-bold'>{singleVideo ? singleVideo.contentTitle : courseContent[0]?.contentTitle}</h2> */}
                    <h2 className='text-3xl font-bold'>{courseContent?.contentTitle}</h2>
                </div>
                <div className='flex justify-end gap-3 items-center'>
                    <button disabled={index === 0} onClick={handlePrev} className='btn btn-sm btn-outline'>Previous</button>
                    <button disabled={index === courseContent?.length - 1} onClick={handleNext} className='btn btn-sm  btn-warning'>Next</button>
                </div>
            </div>
        </div>
    );
};

export default CourseVideoPlayer;