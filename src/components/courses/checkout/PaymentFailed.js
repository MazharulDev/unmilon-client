import React from 'react';
import { Link } from 'react-router-dom';
import paymentFail from '../../../assets/icon/paymentFail.png'

const PaymentFailed = () => {
    return (
        <div>
            <div className='flex justify-center'>
                <div>
                    <div className='flex justify-center my-10'>
                        <img width={300} src={paymentFail} alt="payment-failed" />
                    </div>
                    <div className=''>
                        <h2 className='text-3xl text-red-500 font-bold text-center'>Failed</h2>
                        <h3 className='text-2xl my-5 text-center'>Unfortunately payment was rejected</h3>
                        <p className='my-4'>Page while be automatically redirected to the main page or click button below</p>
                    </div>
                    <div className='flex justify-center mb-5'>
                        <Link className='btn btn-warning' to="/courses">Done</Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PaymentFailed;