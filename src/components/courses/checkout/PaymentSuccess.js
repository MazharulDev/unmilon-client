import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { BiCheckCircle } from 'react-icons/bi'

const PaymentSuccess = () => {
    const location = useLocation();
    const query = new URLSearchParams(location.search)
    const transId = query.get("transactionId")
    const [purchaseCourse, setPurchaseCourse] = useState({});
    useEffect(() => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/purchase-course/${transId}`)
            .then(res => res.json())
            .then(data => setPurchaseCourse(data.data))
    }, [transId])
    if (!purchaseCourse?._id) {
        return (
            <div>
                <h2 className='text-center text-red-500 text-3xl my-10'>No purchase course Found</h2>
            </div>
        )
    }
    console.log(purchaseCourse);
    const { courseName, coursePrice, name, email, phoneNumber, transactionId } = purchaseCourse
    return (
        <div className='container w-4/5 mx-auto shadow-xl p-10 my-10 rounded-lg'>
            <h2 className='text-center text-3xl mt-10 text-green-500 font-bold'>Congratulation Payment is succeessful!!</h2>
            <div className='flex justify-center mt-10'>
                <BiCheckCircle className='text-8xl text-green-500' />
            </div>
            <div className='mt-14'>
                <div className='flex justify-between  font-bold text-xl'>
                    <p>Purchase Course</p>
                    <p>{courseName}</p>
                </div>
                <div className='flex justify-between mt-4 font-bold text-xl'>
                    <p>Name</p>
                    <p>{name}</p>
                </div>
                <div className='flex justify-between mt-4 font-bold text-xl'>
                    <p>Email</p>
                    <p>{email}</p>
                </div>
                <div className='flex justify-between mt-4 font-bold text-xl'>
                    <p>Phone Number</p>
                    <p>{phoneNumber}</p>
                </div>
                <div className='flex justify-between mt-4 my-3 font-bold text-2xl'>
                    <p>Total Paid</p>
                    <p>{coursePrice} &#2547;</p>
                </div>
                <div className='flex justify-between mt-4 font-bold text-xl'>
                    <p>Transaction Id</p>
                    <p>{transactionId}</p>
                </div>
                <div className='flex justify-center items-center gap-4 mt-8 mb-4 print:hidden'>
                    <button className='btn btn-success btn-sm' onClick={() => window.print()}>Print</button>
                    <Link to="/my-courses" className='btn btn-warning btn-sm'>Close</Link>
                </div>
            </div>
        </div>
    );
};

export default PaymentSuccess;