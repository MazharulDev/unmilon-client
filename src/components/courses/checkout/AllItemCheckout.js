import React from 'react';
import primaryTeacher from "../../../assets/primaryTeacherRequitment/primaryTeacherRequitment.png"

const AllItemCheckout = ({ course }) => {
    return (
        <div className=''>
            <div class=" bg-white shadow-lg rounded-lg">
                <div class="p-3">
                    <div class="overflow-x-auto">
                        <table class="table-auto w-full">
                            <thead class="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                <tr>
                                    <th class="p-2 whitespace-nowrap">
                                        <div class="font-semibold text-left">ITEM NAME</div>
                                    </th>
                                    <th class="p-2 whitespace-nowrap">
                                        <div class="font-semibold text-left">AMOUNT</div>
                                    </th>

                                </tr>
                            </thead>
                            <tbody class="text-sm divide-y divide-gray-100">
                                <tr>
                                    <td class="p-2 whitespace-nowrap w-14">
                                        <div class="">
                                            <img className='rounded-md mb-2' width={80} src={primaryTeacher} alt="" />
                                            <div class="font-medium text-gray-800">{course?.title}</div>
                                        </div>
                                    </td>

                                    <td class="p-2 whitespace-nowrap w-8">
                                        <div class="text-left font-medium text-green-500">{course?.price}</div>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AllItemCheckout;