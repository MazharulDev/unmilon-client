import React, { useEffect } from 'react';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import AllItemCheckout from './AllItemCheckout';
import TotalAmountCheckout from './TotalAmountCheckout';

const Checkout = () => {
    const { id } = useParams()
    const [course, setCourse] = useState({})
    const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/${id}`
    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then(result => setCourse(result.data))
    }, [url])
    return (
        <div>
            <div className='container mx-auto px-5 my-10'>
                <div className='mt-5'>
                    <h2 className='text-3xl font-body  my-2 text-center'>Payment</h2>
                    <div className='border-b-2 border-slate-600'></div>
                </div>
                <div className='md:grid grid-cols-2 gap-10 mx-auto'>
                    <div className='mx-5'>
                        <div className='my-5'>
                            <h2 className='text-xl font-bold'>All Items</h2>
                        </div>
                        <AllItemCheckout course={course} />
                    </div>
                    <div className='mx-5'>
                        <TotalAmountCheckout course={course} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Checkout;