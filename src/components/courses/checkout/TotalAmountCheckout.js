import React from 'react';
import auth from '../../../firebase.init';
import { useAuthState } from 'react-firebase-hooks/auth'
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';
import { toast } from 'react-toastify'
import { useForm } from 'react-hook-form';
import { useQuery } from 'react-query';

const TotalAmountCheckout = ({ course }) => {
    const [user, loading, error] = useAuthState(auth);
    const { register, formState: { errors }, handleSubmit } = useForm();
    const { data: userInfo, isLoading, refetch } = useQuery('userInfo', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/userprofile?email=${user?.email}`, {
        method: 'GET',
    })
        .then(res => res.json()))
    if (loading || isLoading) {
        return <LoadingSpinner />
    }
    refetch();
    const info = userInfo?.data[0];
    const onSubmit = async data => {
        const checkoutInfo = {
            name: info?.name,
            phoneNumber: info?.phoneNumber,
            email: user?.email,
            currency: data.currency,
            postcode: data.postcode,
            address: data.address,
            courseId: course._id,
            coursePrice: course.price,
            courseName: course.title,


        }

        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/payment`, {
            method: 'POST',
            headers: {
                "content-type": 'application/json'
            },
            body: JSON.stringify(checkoutInfo)
        })
            .then(res => res.json())
            .then(result => {
                window.location.replace(result?.data)
            })

    }
    if (loading) {
        return <LoadingSpinner />
    }
    if (error) {
        toast.error(error.message)
    }
    return (
        <div className='w-full'>
            <div className='flex justify-between items-center my-5 text-xl font-bold'>
                <h2>Total Amount</h2>
                <p>&#2547; {course?.price}</p>
            </div>

            <div>
                <form className='w-fit mx-auto' onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-control w-full">
                        <label className="label">
                            <span className="label-text">Your full name?</span>
                        </label>
                        <input type="text" value={info?.name} placeholder="Type here" className="input input-bordered max-w-xs" disabled />
                    </div>
                    <label className="label">
                        {errors.name?.type === 'required' && <span className="label-text-alt text-red-500">{errors.name.message}</span>}
                        {errors.name?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.name.message}</span>}
                    </label>
                    <div className='flex justify-between items-center gap-5'>
                        <div className="form-control max-w-xs">
                            <label className="label">
                                <span className="label-text">Your phone number?</span>
                            </label>
                            <input type="text" value={info?.phoneNumber} placeholder="Type here" className="input input-bordered max-w-xs" disabled />
                        </div>
                        <label className="label">
                            {errors.phoneNumber?.type === 'required' && <span className="label-text-alt text-red-500">{errors.phoneNumber.message}</span>}
                            {errors.phoneNumber?.type === 'minLength' && <span className="label-text-alt text-red-500">{errors.phoneNumber.message}</span>}
                        </label>
                        <div className="form-control max-w-xs">
                            <label className="label">
                                <span className="label-text">Your email?</span>
                            </label>
                            <input type="text" value={user?.email} placeholder="Type here" className="input input-bordered max-w-xs" disabled />
                        </div>
                    </div>
                    <div className='flex justify-between items-center gap-5'>
                        <div className="form-control max-w-xs w-full">
                            <label className="label">
                                <span className="label-text">Select your currency</span>
                            </label>
                            <select className="select select-bordered max-w-xs"
                                {...register("currency",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Currency select is Required'
                                        }
                                    }
                                )}
                            >
                                <option disabled>Select your currency</option>
                                <option value="BDT" selected>BDT</option>
                                <option value="USD">USD</option>
                            </select>
                        </div>
                        <label className="label">
                            {errors.currency?.type === 'required' && <span className="label-text-alt text-red-500">{errors.currency.message}</span>}
                        </label>
                        <div className="form-control max-w-xs">
                            <label className="label">
                                <span className="label-text">Your post code?</span>
                            </label>
                            <input type="text" placeholder="Type post code" className="input input-bordered max-w-xs"
                                {...register("postcode",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Postcode is Required'
                                        }
                                    }
                                )}
                            />
                        </div>
                        <label className="label">
                            {errors.postcode?.type === 'required' && <span className="label-text-alt text-red-500">{errors.postcode.message}</span>}
                        </label>
                    </div>
                    <div className=''>
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">Your address</span>
                            </label>
                            <textarea className="textarea textarea-bordered h-24 w-full" placeholder="Type your current address"
                                {...register("address",
                                    {
                                        required: {
                                            value: true,
                                            message: 'Address is Required'
                                        }
                                    }
                                )}
                            ></textarea>
                        </div>
                        <label className="label">
                            {errors.address?.type === 'required' && <span className="label-text-alt text-red-500">{errors.address.message}</span>}
                        </label>
                    </div>
                    <input type="submit" className='btn btn-success w-full text-white mt-5' value="পে করুন " />
                </form>
            </div>
        </div>
    );
};

export default TotalAmountCheckout;