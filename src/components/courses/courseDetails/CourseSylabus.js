import React, { useEffect, useState } from 'react';
import { AiFillPlayCircle } from 'react-icons/ai';
import './sylabus.css'

const CourseSylabus = ({ title }) => {
    const [months, setMonths] = useState('')
    const [subjects, setSubjects] = useState([])
    const [courseContent, setCourseContent] = useState([])
    const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month?coursename=${title}`
    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then(result => setCourseContent(result?.data))
    }, [url])
    useEffect(() => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/subject?coursename=${title}&month=${months}`)
            .then(res => res.json())
            .then(result => setSubjects(result?.data))
    }, [months, title])
    return (
        <div>
            <div className='mb-5 mt-10'>
                <h2 className='text-2xl'>কোর্স সিলেবাস</h2>
            </div>
            {/* {
                courseContent?.map(content => (
                    <div key={content._id} className='mb-2'>
                        <div tabIndex={0} className="collapse collapse-plus border border-base-300 bg-base-100 rounded-box">
                            <input onClick={() => setMonths(content?.month)} type="checkbox" />
                            <div onClick={() => setMonths(content?.month)} className="collapse-title text-xl font-medium">
                                {content?.month}
                            </div>
                            <div className="collapse-content">
                                {
                                    subjects?.map(subject => (
                                        <div key={subject._id} className='flex justify-start gap-2 items-center cursor-pointer mt-2 pl-4'>
                                            <AiFillPlayCircle className='text-2xl text-gray-500' />
                                            <p>{subject?.subject}</p>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                ))
            } */}
            <div>
                {
                    courseContent?.map((content, index) => (
                        <section key={content._id}>



                            <div className="ac">

                                <input className="ac-input" id={`ac-${index + 1}`} name={`ac-${index + 1}`} type="checkbox" />
                                <label onClick={() => setMonths(content?.month)} className="ac-label" for={`ac-${index + 1}`}>{content?.month}</label>

                                <article className="ac-text">
                                    {
                                        subjects?.map((subject) => (
                                            <div key={subject._id} className="ac-sub">
                                                <input className="ac-input" id={`ac-${index + 300}`} name={`ac-${index + 300}`} type="checkbox" />
                                                <label className="ac-label" for={`ac-${index + 300}`}>{subject?.subject},{index + 300}</label>
                                                <article className="ac-sub-text">
                                                    <p>But not only is the sea such a foe to man who is an alien to it, but it is also a fiend to its own off-spring;</p>
                                                </article>
                                            </div>
                                        ))
                                    }

                                </article>

                            </div>
                        </section>
                    ))
                }
            </div>

        </div>
    );
};

export default CourseSylabus;