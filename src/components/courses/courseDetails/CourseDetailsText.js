import React from 'react';
import CourseSylabus from './CourseSylabus';

const CourseDetailsText = ({ courseDetails, id }) => {
    const { title, status, description } = courseDetails;
    return (
        <div>
            <div>
                <h2 className='text-4xl'>{title}</h2>
                <p className='text-slate-600 my-5'>{status}</p>
                <h4 className='text-xl font-semibold text-slate-600 my-3'>এই কোর্সটি থেকে যা শিখবেন:</h4>
            </div>
            <div className='p-3 border border-slate-500 rounded-md'>
                <div
                    dangerouslySetInnerHTML={{ __html: description }}
                />
            </div>
            <CourseSylabus title={title} />
            <div>
                <h2 className='mt-10 mb-5 text-2xl'>হেল্প লাইন</h2>
                <div className='p-5 bg-white'>
                    <p>কোস সংক্রান্ত যে কোনো সমস্যায় যোগাযোগ করুন 01300000000</p>
                </div>
            </div>
            <div>
                <h2 className='mt-16 mb-5 text-2xl'>যেভাবে পেমেন্ট করবেন</h2>
                <div className='p-5 border border-l-stone-600'>
                    <p>কীভাবে পেমেন্ট করবেন তা বিস্তারিত জানতে <span className='text-blue-500 underline'>এই ভিডিও দেখুন</span></p>
                </div>
            </div>
        </div>
    );
};

export default CourseDetailsText;