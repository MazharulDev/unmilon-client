import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import PrimaryCourses from '../../home/CoursesCategoriesDivide/PrimaryCourses';
import CourseDetailsShow from './CourseDetailsShow';
import CourseDetailsText from './CourseDetailsText';

const CourseDetails = () => {
    const { id } = useParams();
    const [courseDetails, setCourseDetails] = useState({})
    const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/${id}`
    useEffect(() => {
        fetch(url)
            .then(res => res.json())
            .then(result => setCourseDetails(result.data))
    }, [url])
    return (
        <div className='container mx-auto'>
            <div className='flex flex-col-reverse md:flex md:flex-row justify-between items-start p-10 gap-10 mx-auto'>
                <div className=''>
                    <CourseDetailsText courseDetails={courseDetails} id={id} />
                </div>
                <div className=''>
                    <CourseDetailsShow courseDetails={courseDetails} />
                </div>
            </div>
            <PrimaryCourses />
        </div>
    );
};

export default CourseDetails;