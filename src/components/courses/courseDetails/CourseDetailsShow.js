import React, { useState } from 'react';
import { MdLocalOffer } from 'react-icons/md';
import { RxArrowTopRight } from 'react-icons/rx';
import { Link } from 'react-router-dom';
import playIcon from '../../../assets/icon/di9rnbxjT.gif'
// import getYouTubeID from 'get-youtube-id';

const CourseDetailsShow = ({ courseDetails }) => {
    // // const getYouTubeID = require('get-youtube-id');

    // const id = getYouTubeID("https://youtu.be/DG5MJrJ1jjY");
    // console.log(id);

    const [intro, setIntro] = useState(true);
    return (
        <div>

            <div className="w-[350px] lg:w-[30rem] h-[30rem] border-2 border-yellow-500 rounded-2xl overflow-hidden">
                <div className='w-full overflow-hidden'>
                    {/* onClick={() => setIntro(false)} */}
                    {
                        intro === true ? <div className='relative'>
                            <div onClick={() => setIntro(false)} className='w-full h-full flex justify-center items-center mx-auto absolute z-10 cursor-pointer '>
                                <img className='w-16 bg-slate-600 rounded-full p-1 brightness-200 hover:scale-105' src={playIcon} alt="" />
                            </div>
                            <img className='rounded-t-2xl brightness-50' src={courseDetails?.img} alt="" />

                        </div> :
                            // https://youtube.com/embed/
                            <iframe width="480" height="280" src={`https://youtube.com/embed/${courseDetails?.courseIntro}`} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    }
                </div>
                <div className='overflow-hidden bg-white pt-5 p-3 h-32'>
                    <div>
                        <div className='flex justify-between items-center'>
                            <div className='flex justify-start items-center gap-2 text-yellow-800'>
                                <MdLocalOffer />
                                <p>Special Offer: <span className='text-red-500'>Ended</span></p>
                            </div>
                            <div className='flex justify-start items-center gap-2 text-green-500 pt-2'>
                                <RxArrowTopRight />
                                <p>Ongoing</p>
                            </div>
                        </div>


                    </div>

                    <p className='my-3'>{courseDetails.status}</p>
                    <div className='flex justify-between items-center'>
                        <button className='underline'>প্রোমো কোড </button>
                        <p className='font-bold text-blue-500 text-lg'>
                            {
                                courseDetails.price === "free" ? courseDetails.price : <p> &#2547; {courseDetails.price} </p>
                            }
                        </p>
                    </div>
                </div>
                <div className='bg-white h-full'>
                    <div className='flex justify-center items-center gap-4 px-2 py-3 w-full '>
                        <Link to={`/checkout/${courseDetails._id}`} className='bg-[#ffcd33] hover:bg-[#daa814] rounded-lg px-5 py-2 w-full font-bold duration-300'>কোর্সটি কিনুন  </Link>
                        <button className='bg-green-400 hover:bg-green-500 rounded-lg px-5 py-2 w-full font-bold duration-300'>ফ্রি এনরোল রিকুয়েস্ট </button>
                    </div>
                </div>
            </div>


        </div>
    );
};

export default CourseDetailsShow;