import React from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useQuery } from 'react-query';
import { Link } from 'react-router-dom';
import auth from '../../../firebase.init';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';

const MyCourses = () => {
    const [user, loading] = useAuthState(auth);

    const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/my-course/${user?.email}`
    const { data: myCourse, isLoading, refetch } = useQuery('myCourses', () => fetch(url, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));
    refetch()
    // console.log(myCourse);
    if (loading || isLoading) {
        return <LoadingSpinner />
    }
    return (
        <div className='my-5 mb-20'>
            {
                myCourse?.data?.length === 0 ?
                    <div className='my-20'>
                        <h2 className='text-5xl text-center text-red-500'>You have not purchased any courses</h2>
                    </div> :
                    <>
                        <div>
                            <h2 className='text-3xl text-center font-bold my-10'>আমার কোর্স সমূহ</h2>
                        </div>
                        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 w-fit gap-10 mx-auto'>
                            {
                                myCourse?.data?.map(course => (
                                    <div key={course?._id}>

                                        <div className="w-96 border-2 border-yellow-500 rounded-2xl hover:scale-105 duration-500 overflow-hidden">
                                            <div className='w-96 overflow-hidden'>
                                                <img className='rounded-t-2xl  hover:cursor-pointer' src={course?.courseImg} alt="" />
                                            </div>
                                            <div className='overflow-hidden bg-white pt-5 p-3 h-16'>
                                                {course?.courseName.length > 38 ? <h3 className='text-xl font-bold text-yellow-700'>{course?.courseName.slice(0, 38)}...</h3> : <h3 className='text-xl font-bold text-yellow-700'>{course?.courseName}</h3>}

                                            </div>
                                            <div className='border-t-2 border-yellow-400 w-full h-full flex justify-center bottom-0'>
                                                {
                                                    course?.courseContent?.length >= 1 && <div className='flex justify-between items-center p-4 py-2'>
                                                        <Link to={`/content-show/${course?._id}`} className='bg-[#daa814] hover:bg-[#b68a06] px-4 py-2 font-bold rounded-lg duration-200'>ক্লাস কন্টিনিউ করুন </Link>
                                                        <p className='h-full bottom-0'>

                                                        </p>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </>
            }

        </div>
    );
};

export default MyCourses;