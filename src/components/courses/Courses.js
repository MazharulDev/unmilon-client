import React from 'react';
import PopularCourses from '../home/CoursesCategoriesDivide/PopularCourses';
import PrimaryCourses from '../home/CoursesCategoriesDivide/PrimaryCourses';


const Courses = () => {
    return (
        <div>
            <PrimaryCourses />
            <PopularCourses />
        </div>
    );
};

export default Courses;