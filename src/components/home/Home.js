import React from 'react';
import Banner from './Banner';
import Books from './Books';
import Categories from './categories/Categories';
import PopularCourses from './CoursesCategoriesDivide/PopularCourses';
import PrimaryCourses from './CoursesCategoriesDivide/PrimaryCourses';
import ReviewSection from './ReviewSection';
import Schedule from './Schedule';


const Home = () => {
    return (
        <div>
            <Banner />
            <Schedule />
            <Categories />
            <PrimaryCourses />
            <Books />
            <PopularCourses />
            <ReviewSection />
        </div>
    );
};

export default Home;