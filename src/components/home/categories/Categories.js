import React, { useEffect, useState } from 'react';
import { Link, Outlet } from 'react-router-dom';
import bcs from '../../../assets/images/bcs.jpg'
import primaryTeacher from '../../../assets/images/primaryTeacher.jpg'
import bankjob from '../../../assets/images/bankjob.jpg'
import jobexam from "../../../assets/images/jobexam.png"
import teacheraddmission from "../../../assets/images/teacheradmission.jpg"
import basicMath from "../../../assets/images/basicMath.jpg"
import virsityAddmission from "../../../assets/images/versityAdmisstion.jpg"
import englishLanguage from "../../../assets/images/englishLanguage.jpg"
import skillDevelopment from "../../../assets/images/skillDevelopment.jpg"
import CategoriesCard from './CategoriesCard';

const Categories = () => {
    const projectsNav = [
        { name: "অনলাইন কোর্সসমূহ" },
        { name: "অফলাইন কোর্স" },
        { name: "রেকর্ডেড কোর্সসমূহ" },
        { name: "লাইভ এক্সাম" },
    ];

    const projectsData = [
        { id: "1", name: "বিসিএস ", img: bcs, category: "অনলাইন কোর্সসমূহ" },
        { id: "2", name: "ব্যাংক জব ", img: bankjob, category: "অনলাইন কোর্সসমূহ" },
        { id: "3", name: "জব এক্সাম", img: jobexam, category: "অনলাইন কোর্সসমূহ" },
        { id: "4", name: "শিক্ষক নিবন্ধন", img: teacheraddmission, category: "অনলাইন কোর্সসমূহ" },
        { id: "5", name: "বেসিক ম্যাথ ", img: basicMath, category: "অনলাইন কোর্সসমূহ" },
        { id: "6", name: "বিসিএস ", img: bcs, category: "অনলাইন কোর্সসমূহ" },
        { id: "7", name: "প্রাইমারী শিক্ষক ", img: primaryTeacher, category: "অনলাইন কোর্সসমূহ" },
        { id: "8", name: "ব্যাংক জব ", img: bankjob, category: "অফলাইন কোর্স" },
        { id: "9", name: "জব এক্সাম ", img: jobexam, category: "অফলাইন কোর্স" },
        { id: "10", name: "শিক্ষক নিবন্ধন ", img: teacheraddmission, category: "অফলাইন কোর্স" },
        { id: "11", name: "বেসিক ম্যাথ ", img: basicMath, category: "রেকর্ডেড কোর্সসমূহ" },
        { id: "11", name: "জব এক্সাম ", img: jobexam, category: "রেকর্ডেড কোর্সসমূহ" },
        { id: "12", name: "ভার্সিটি ভর্তি", img: virsityAddmission, category: "রেকর্ডেড কোর্সসমূহ" },
        { id: "13", name: "ইংলিশ ল্যাংগুয়েজ ", img: englishLanguage, category: "রেকর্ডেড কোর্সসমূহ" },
        { id: "14", name: "স্কিল ডেভেলপমেন্ট ", img: skillDevelopment, category: "লাইভ এক্সাম" }

    ];

    const [item, setItem] = useState({ name: 'অনলাইন কোর্সসমূহ' });
    const [projects, setProjects] = useState([]);
    const [active, setActive] = useState(0);

    useEffect(() => {
        // get projects based on item
        if (item.name === 'all') {
            setProjects(projectsData);
        } else {
            const newProjects = projectsData.filter((project) => {
                return project.category.toLowerCase() === item.name;
            });
            setProjects(newProjects);
        }
    }, [item]);

    const handleClick = (e, index) => {
        setItem({ name: e.target.textContent.toLowerCase() });
        setActive(index);
    };
    return (
        <div className='container mx-auto'>
            {/* projects nav */}
            <nav className='mb-5 bg-blue-100 w-4/5 mx-auto rounded-xl mt-5'>
                <ul className='flex flex-col md:flex-row justify-evenly items-center'>
                    {projectsNav.map((item, index) => {
                        return (
                            <li
                                onClick={(e) => {
                                    handleClick(e, index);
                                }}
                                className={`${active === index ? 'bg-white border-b-2 border-blue-800' : ''
                                    } cursor-pointer capitalize m-4 hover:bg-white px-6 py-3 rounded-lg`}
                                key={index}
                            >
                                {item.name}
                            </li>
                        );
                    })}
                </ul>
            </nav>
            {/* projects */}
            <section className='grid grid-cols-1 md:grid-cols-3 gap-y-6 lg:grid-cols-5 lg:gap-x-8 lg:gap-y-8 mt-5 w-4/5 mx-auto'>
                {projects.map((item) => {
                    return <CategoriesCard item={item} />
                })}
            </section>
        </div>
    );
};

export default Categories;