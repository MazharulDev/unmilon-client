import React from 'react';

const CategoriesCard = ({ item }) => {
    return (
        <div className='container mx-auto mt-5'>
            <div>
                <div className='w-48 bg-white p-5 text-center rounded-2xl shadow-lg mx-auto mb-10 hover:bg-slate-100 cursor-pointer'>
                    <img src={item.img} alt="" />
                    <p className='text-xl mt-4'>{item.name}</p>
                </div>
            </div>
        </div>
    );
};

export default CategoriesCard;