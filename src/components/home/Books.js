import React from 'react';
import Slider from 'react-slick';

import banglaHomeTeacher from "../../assets/books/banglaHomeTeacher.jpg"

// arraw button style 

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "black", borderRadius: "50%", }}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "black", borderRadius: "50%" }}
            onClick={onClick}
        />
    );
}
const Books = () => {
    // book data 
    const books = [
        { name: "বিসিএস প্রিলিমিনারি গার্হস্থ্য শিক্ষক বাংলা বই ", price: 250.00, writter: "মোস্তাফিজুর রহমান", img: banglaHomeTeacher },
        { name: "বিসিএস প্রিলিমিনারি গার্হস্থ্য শিক্ষক বাংলা বই ", price: 350.00, writter: "মোস্তাফিজুর রহমান", img: banglaHomeTeacher },
        { name: "বিসিএস প্রিলিমিনারি গার্হস্থ্য শিক্ষক বাংলা বই ", price: 450.00, writter: "মোস্তাফিজুর রহমান", img: banglaHomeTeacher },
        { name: "বিসিএস প্রিলিমিনারি গার্হস্থ্য শিক্ষক বাংলা বই ", price: 550.00, writter: "মোস্তাফিজুর রহমান", img: banglaHomeTeacher },
        { name: "বিসিএস প্রিলিমিনারি গার্হস্থ্য শিক্ষক বাংলা বই ", price: 550.00, writter: "মোস্তাফিজুর রহমান", img: banglaHomeTeacher },
        { name: "বিসিএস প্রিলিমিনারি গার্হস্থ্য শিক্ষক বাংলা বই খুব ভালো বই ", price: 550.00, writter: "মোস্তাফিজুর রহমান", img: banglaHomeTeacher }
    ]
    // slider function 
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        cssEase: "linear",
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 3,
        slidesToScroll: 2,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    return (
        <div className='bg-orange-100 px-8 my-10 py-5 mx-auto mb-10'>
            <div className='container mx-auto'>
                <h2 className='text-3xl text-center font-bold my-10'>বইসমূহ </h2>
                <Slider {...settings}>
                    {
                        books.map((book, index) => (
                            <div className="px-5 py-2 bg-white shadow-lg">
                                <div className="">
                                    <div className='overflow-hidden'>
                                        <img className='w-full h-full hover:scale-105 duration-300 cursor-pointer' src={book.img} alt="" />
                                    </div>
                                    <h2 className='text-lg font-bold my-3'>{book.name.slice(0, 38)}</h2>
                                    <h2 className='text-center text-slate-700 font-bold'>{book.writter}</h2>
                                </div>
                                <div className="text-center text-xl my-3 font-bold text-blue-500">
                                    <h3>&#2547; {book.price}</h3>
                                </div>
                            </div>
                        ))
                    }
                </Slider>
            </div>
            <div className='flex justify-center my-10'>
                <button className='bg-[#daa814] px-6 py-2 rounded-lg hover:bg-[#b68a06] duration-200 font-bold'>সবগুলো বই দেখুন </button>
            </div>
        </div>
    );
};

export default Books;