import React from 'react';
import Slider from 'react-slick';
import { FaPlay, FaQuoteLeft, FaQuoteRight } from 'react-icons/fa'

// arraw button style 

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "black", borderRadius: "50%" }}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "black", borderRadius: "50%" }}
            onClick={onClick}
        />
    );
}

const ReviewSection = () => {
    // book data 
    const data = [
        { id: 1, name: "Shoriful Islam", position: "CEO", companyName: "Xozosoft Ltd" },
        { id: 2, name: "Mazharul Islam", position: "Web Programmer", companyName: "Xozosoft Ltd" },
        { id: 3, name: "Faisal Ahmad", position: "Web Developer", companyName: "Web-Solutions" },
        { id: 4, name: "Badhon", position: "Web Developer", companyName: "TechSoft Ltd" },
        { id: 5, name: "Badhon", position: "Web Developer", companyName: "TechSoft Ltd" },
    ]
    // slider function 
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        cssEase: "linear",
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 3,
        slidesToScroll: 2,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    return (
        <div className='bg-orange-100 px-8 my-10 py-10 mx-auto mb-10'>
            <div className='container mx-auto'>
                <h2 className='text-3xl text-center font-bold my-10'>রিভিউ</h2>
                <Slider {...settings}>
                    {
                        data.map(info => (
                            <div key={info.id} className="px-5 py-2 bg-blue-100 shadow-lg rounded-xl cursor-pointer">
                                <div className="pb-8">
                                    <div className='overflow-hidden'>
                                        <div className='flex justify-start items-center'>
                                            <FaQuoteLeft className='text-2xl text-yellow-500' />
                                            <h3 className='text-xl mt-8 ml-2 text-slate-700'>{info.companyName}</h3>
                                            <FaQuoteRight className='text-2xl mt-14 ml-2 text-yellow-500' />
                                        </div>
                                        <div className='flex justify-start items-center ml-3 mt-5'>
                                            <div className='p-5 bg-white rounded-full'>
                                                <FaPlay />
                                            </div>
                                            <div className='ml-3'>
                                                <h2 className='text-2xl font-bold text-slate-600'>{info.name}</h2>
                                                <p className='text-xl text-slate-700'>{info.position}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        ))
                    }
                </Slider>
            </div>
        </div>


    );
};

export default ReviewSection;