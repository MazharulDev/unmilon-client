import React from 'react';
import bannerImg from '../../assets/images/output-onlinegiftools.gif'
import playstoreicon from '../../assets/icon/playstoreicon.png'

const Banner = () => {
    return (
        <section className='bg-gradient-to-b from-indigo-700 via-indigo-200 to-white lg:bg-gradient-to-r lg:from-indigo-700 lg:via-indigo-200 lg:to-white py-7 md:mt-0'>
            <div className='container mx-auto'>
                <div className='md:grid grid-cols-2 w-full items-center mx-auto px-5'>
                    <div>
                        <h2 className='text-5xl font-bold text-white'>শুরু থেকে শুরু করুন, বেসিক থেকে এডভান্সড পর্যন্ত</h2>
                        <h3 className='text-2xl font-bold my-3 text-white'>সফলতার জন্য প্রয়োজন সদিচ্ছা, একাগ্রতা ও সঠিক দিক নির্দেশনা </h3>
                        <h3 className='my-3 text-white'>জীবন একটাই,যেখানে ভূল সিদ্ধান্ত নেওয়ার সুযোগ নেই</h3>

                        {/* <p className='text-2xl my-7 text-gray-800'>নতুন কিছু শিখতে চান? বেসিক থেকে শুরু করুন এবং এডভান্সড লেভেলের দক্ষতা অর্জন করুন, সফলতা আসবেই ইনশাল্লাহ।</p> */}
                        <button className='bg-gradient-to-r from-blue-500 to-green-400 hover:from-green-400 hover:to-blue-500 rounded-full px-5 py-2 flex justify-start items-center gap-2 transition ease-in-out delay-150'><img className='' width={35} src={playstoreicon} alt="icon" /><a className='font-bold' href="#">  অ্যাপটি ডাউনলোড করুন</a></button>
                    </div>
                    <div className='flex justify-center md:justify-end'>
                        <img className='w-[400px]' src={bannerImg} alt="banner images" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Banner;