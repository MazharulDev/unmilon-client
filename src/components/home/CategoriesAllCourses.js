import React from 'react';
import { MdLocalOffer } from 'react-icons/md';
import { RxArrowTopRight } from 'react-icons/rx';
import { useQuery } from 'react-query';
import { Link, useParams } from 'react-router-dom';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';

const CategoriesAllCourses = () => {
    const { categories } = useParams()
    const { data: SpacificCategoriesCourses, isLoading, refetch } = useQuery('primaryCourse', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/specific/${categories}`, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));
    refetch()
    if (isLoading) {
        return <LoadingSpinner />
    }
    return (
        <div className='mb-10'>
            <div>
                <h2 className='text-3xl text-center font-bold my-10'>{categories}</h2>
            </div>
            <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 w-fit gap-10 mx-auto'>
                {
                    SpacificCategoriesCourses?.data?.map(course =>
                        <div key={course._id}>



                            <div className="w-96 border-2 border-yellow-500 rounded-2xl overflow-hidden">
                                <div className='w-96 overflow-hidden'>
                                    <img className='rounded-t-2xl hover:scale-110 duration-500 hover:cursor-pointer' src={course.img} alt="" />
                                </div>
                                <div className='overflow-hidden bg-white pt-5 p-3 h-32'>
                                    <h3 className='text-xl font-bold text-yellow-700'>{course.title}</h3>
                                    <div>
                                        <div className='flex justify-between items-center'>
                                            <div className='flex justify-start items-center gap-2 text-yellow-800'>
                                                <MdLocalOffer />
                                                <p>Special Offer: <span className='text-red-500'>Ended</span></p>
                                            </div>
                                            <div className='flex justify-start items-center gap-2 text-green-500 pt-2'>
                                                <RxArrowTopRight />
                                                <p>Ongoing</p>
                                            </div>
                                        </div>


                                    </div>

                                    <p className='my-3'>{course.status}</p>
                                </div>
                                <div className='bg-slate-200 w-full h-full bottom-0'>
                                    <div className='flex justify-between items-center p-4 py-2'>
                                        <Link to={`/course-details/${course._id}`} className='bg-[#daa814] hover:bg-[#b68a06] px-4 py-2 font-bold rounded-lg duration-200'>বিস্তারিত দেখুন </Link>
                                        <p className='h-full bottom-0'>
                                            {
                                                course.price === 0 ? course.price : <p> &#2547; {course.price} </p>
                                            }
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    )
                }
            </div>
        </div>
    );
};

export default CategoriesAllCourses;