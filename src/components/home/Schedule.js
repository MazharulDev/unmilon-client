import React from 'react';
import questionIcon from '../../assets/icon/question-bank.png'
import classSchedule from "../../assets/icon/class-schedule.png"
import todayClass from '../../assets/icon/todays-class.png'
import todayExam from "../../assets/icon/todays-exam.png"

const Schedule = () => {
    const scheduleData = [
        { id: 2, img: classSchedule, text: "ক্লাস সিডিউল " },
        { id: 3, img: todayClass, text: "আজকের ক্লাস " },
        { id: 4, img: todayExam, text: "আজকের পরীক্ষা " },
        { id: 1, img: questionIcon, text: "প্রশ্নব্যাংক " },
    ]
    return (
        <section className='bg-[#faf9f9] p-10'>
            <div className='container mx-auto w-fit'>
                <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-8'>
                    {
                        scheduleData.map(schedule => (
                            <div key={schedule.id} className="flex justify-center items-center gap-2 px-5 py-10 bg-indigo-900 rounded-lg w-72 lg:w-96 shadow-lg hover:bg-[#4f47a7] text-white cursor-pointer mx-auto">
                                <img width={80} src={schedule.img} alt="" />
                                <p className='text-xl'>{schedule.text}</p>
                            </div>
                        ))
                    }
                </div>
            </div>
        </section>
    );
};

export default Schedule;