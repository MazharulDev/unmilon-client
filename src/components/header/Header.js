import React, { useState } from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import { AiOutlineMenu, AiOutlineShoppingCart } from 'react-icons/ai'
import { AiOutlineClose } from 'react-icons/ai'
import SubHeader from './SubHeader';
import logo from '../../assets/logo/logo.jpeg'
import { useAuthState } from 'react-firebase-hooks/auth';
import auth from '../../firebase.init';
import { signOut } from "firebase/auth";
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';
import { toast } from 'react-toastify';
import useAdmin from '../../shared/hooks/useAdmin';
import CustomLink from '../../shared/customLink/CustomLink';
import { useQuery } from 'react-query';

const Header = () => {
    const navigate = useNavigate()
    const [user, loading, error] = useAuthState(auth);
    const [admin] = useAdmin(user)
    const [open, setOpen] = useState(false);

    const { data: userInfo, isLoading, refetch } = useQuery('userInfo', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/userprofile?email=${user?.email}`, {
        method: 'GET',
    })
        .then(res => res.json()))

    if (loading || isLoading) {
        return <LoadingSpinner />
    }
    refetch();
    const info = userInfo?.data[0]
    const handleSignOut = () => {
        signOut(auth).then((user) => {
            if (!user) {
                toast("Sign Out successfully");
                navigate("/login")
            }
        });
    }
    if (loading) {
        return <LoadingSpinner />
    }
    return (
        <div className='sticky top-0 z-30 print:hidden'>
            <SubHeader />
            <nav className='bg-white shadow-md'>
                <div className='flex justify-between items-center container mx-auto'>
                    <div className='flex justify-start items-center py-5 mx-4'>
                        <Link to="/"><img className='w-36' src={logo} alt="logo" /></Link>
                        <div onClick={() => setOpen(!open)}>
                            {
                                open ? <AiOutlineClose className='z-20 fixed right-5 text-2xl top-7 font-bold cursor-pointer md:hidden' /> : <AiOutlineMenu className='z-20 text-2xl fixed right-5 top-20 font-bold cursor-pointer md:hidden' />
                            }
                        </div>
                        <ul className={`bg-white md:pl-4 pr-32 md:pr-10 z-10 md:static fixed top-0 md:h-auto h-screen duration-500 ease-linear shadow-2xl md:shadow-none lg:shadow-none ${!open ? 'right-[-100%]' : 'right-0'}`}>
                            <li onClick={() => setOpen(false)} className="md:inline-block md:ml-10 ml-5 md:my-0 my-6 border-b-2 border-transparent duration-300">
                                <CustomLink className='hover:text-gray-800' to="/">হোম</CustomLink>
                            </li>
                            <li onClick={() => setOpen(false)} className="md:inline-block md:ml-10 ml-5 md:my-0 my-6 border-b-2 border-transparent  duration-300">
                                <CustomLink className='hover:text-gray-800' to="/courses">সকল কোর্স </CustomLink>
                            </li>
                            <li onClick={() => setOpen(false)} className="md:inline-block md:ml-10 ml-5 md:my-0 my-6 border-b-2 border-transparent  duration-300">
                                <CustomLink className='hover:text-gray-800' to="/job-preparation">চাকুরী প্রস্তুতি </CustomLink>
                            </li>
                            <li onClick={() => setOpen(false)} className="md:inline-block md:ml-10 ml-5 md:my-0 my-6 border-b-2 border-transparent  duration-300">
                                <CustomLink className='hover:text-gray-800' to="/addmistion-exam">ভর্তি পরীক্ষা </CustomLink>
                            </li>
                            <li onClick={() => setOpen(false)} className="md:inline-block md:ml-10 ml-5 md:my-0 my-6 border-b-2 border-transparent  duration-300">
                                <CustomLink className='hover:text-gray-800' to="/notice">নোটিশ</CustomLink>
                            </li>
                            {
                                admin && <li onClick={() => setOpen(false)} className="md:inline-block md:ml-10 ml-5 md:my-0 my-6 border-b-2 border-transparent  duration-300">
                                    <CustomLink className='hover:text-gray-800' to="/dashboard">ড্যাশবোর্ড</CustomLink>
                                </li>
                            }
                        </ul>
                    </div>
                    <div className='flex justify-center items-center  gap-3 mr-12 md:mr-0'>
                        {
                            user ?
                                <div className="indicator">
                                    <span className="indicator-item badge ">0</span>
                                    <button className=""><AiOutlineShoppingCart className='text-3xl cursor-pointer hover:text-slate-600' /></button>
                                </div>
                                : <AiOutlineShoppingCart className='text-3xl cursor-pointer hover:text-slate-600' />
                        }
                        {
                            user ?
                                <div className="dropdown dropdown-end">
                                    <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
                                        <div className="w-10 rounded-full">
                                            {info?.imgURL ? <img src={info?.imgURL} alt='' /> : <div className='text-3xl h-full text-white bg-primary flex justify-center items-center'>{user.displayName ? user?.displayName?.substring(0, 1) : 'U'}</div>}
                                        </div>
                                    </label>
                                    <ul tabIndex={0} className="mt-3 p-2 shadow menu menu-compact dropdown-content bg-base-100 rounded-box w-52">
                                        <li>
                                            <Link to="/profile" className="justify-between">
                                                প্রোফাইল
                                            </Link>
                                        </li>
                                        <li><Link to='/my-courses'>আমার কোর্স সমূহ </Link></li>
                                        <li><button onClick={handleSignOut}>লগআউট</button></li>
                                    </ul>
                                </div>
                                : <Link to="/login" className='px-4 py-2 border rounded-md border-yellow-500 hover:bg-yellow-200 font-bold'>লগইন</Link>
                        }
                        {/* <CgProfile /> */}
                    </div>
                </div>
            </nav>
        </div>
    );
};

export default Header;