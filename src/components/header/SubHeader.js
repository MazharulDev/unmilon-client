import React from 'react';
import { IoMdCall } from "react-icons/io"
import { GrMail } from 'react-icons/gr'
import { BsFacebook } from 'react-icons/bs'
import { BsMessenger } from 'react-icons/bs'
import { BsWhatsapp } from 'react-icons/bs'
import { BsYoutube } from 'react-icons/bs'
// import { BsSearch } from 'react-icons/bs'



const SubHeader = () => {
    return (
        <nav className='bg-indigo-300 print:hidden'>
            <div className='flex justify-between items-center container mx-auto'>
                <div className='hidden md:flex justify-center items-center gap-3'>
                    <IoMdCall className='text-xl font-bold' />
                    <p>01717-355184</p>
                    <GrMail className='text-xl font-bold' />
                    <p>Email: unmilan2011@gmail.com</p>
                </div>
                <div className='flex justify-center items-center gap-2 text-3xl text-white font-bold p-3 mx-auto md:mx-0'>
                    <a href="#"><BsFacebook className='bg-[#15ade2] p-2 rounded-md border border-white hover:scale-105' /></a>
                    <a href="#"><BsMessenger className='bg-[#15ade2] p-2 rounded-md border border-white hover:scale-105' /></a>
                    <a href="#"><BsWhatsapp className='bg-[#01e7a2] p-2 rounded-md border border-white hover:scale-105' /></a>
                    <a href="#"><BsYoutube className='bg-[#ff0049] p-2 rounded-md border border-white hover:scale-105' /></a>
                    {/* <button><BsSearch className='bg-gray-400 p-2 rounded-lg border border-white ml-3 hover:scale-105' /></button> */}
                </div>
            </div>
        </nav>
    );
};

export default SubHeader;