import React, { useState } from 'react';
import { BiEdit } from 'react-icons/bi';
import { RiDeleteBin6Fill } from 'react-icons/ri';
import { useQuery } from 'react-query';
import { Link } from 'react-router-dom';
import SubjectDelete from '../../../shared/modal/SubjectDelete';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';

const SubjectShow = ({ courseName, courseMonths }) => {
    const [deleteSubject, setDeleteSubject] = useState(null)
    const { data: subjects, isLoading, refetch } = useQuery('subjectsShow', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/subject?coursename=${courseName}&month=${courseMonths}`, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));

    if (isLoading) {
        return <LoadingSpinner />
    }
    refetch()
    return (
        <div className='my-10'>
            <table className="table w-2/4 mx-5">
                <thead>
                    <tr>
                        <th></th>
                        <th>Categories</th>
                        <th>Action</th>


                    </tr>
                </thead>
                <tbody>
                    {
                        subjects?.data?.map((subject, index) =>
                            <tr key={subject._id}>
                                <th>{index + 1}</th>
                                <td>{subject.subject}</td>
                                <td className='flex justify-start items-center gap-4 py-8'>
                                    <Link to={`/dashboard/update-subject/${subject._id}`}><BiEdit className='text-green-500 text-3xl hover:text-green-700 cursor-pointer' /></Link>
                                    <label onClick={() => setDeleteSubject(subject)} htmlFor="monthDelete">
                                        <RiDeleteBin6Fill className='text-red-500 text-3xl hover:text-red-700 cursor-pointer' />
                                    </label>
                                </td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
            {
                deleteSubject && <SubjectDelete deleteSubject={deleteSubject} refetch={refetch} setDeleteSubject={setDeleteSubject} />
            }
        </div>
    );
};

export default SubjectShow;