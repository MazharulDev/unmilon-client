import React, { useState } from 'react';
import { BiEdit } from 'react-icons/bi';
import { RiDeleteBin6Fill } from 'react-icons/ri';
import { useQuery } from 'react-query';
import { Link } from 'react-router-dom';
import MonthDelete from '../../../shared/modal/MonthDelete';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';

const ContentMonthShow = ({ courseName }) => {
    const [deleteMonth, setDeleteMonth] = useState(null)
    const { data: months, isLoading, refetch } = useQuery('monthShow', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month?coursename=${courseName}`, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));

    if (isLoading) {
        return <LoadingSpinner />
    }
    refetch()
    return (
        <div className='my-10'>
            <table className="table w-2/4 mx-5">
                <thead>
                    <tr>
                        <th></th>
                        <th>Categories</th>
                        <th>Action</th>


                    </tr>
                </thead>
                <tbody>
                    {
                        months?.data?.map((month, index) =>
                            <tr key={month._id}>
                                <th>{index + 1}</th>
                                <td>{month.month}</td>
                                <td className='flex justify-start items-center gap-4 py-8'>
                                    <Link to={`/dashboard/update-month/${month._id}`}><BiEdit className='text-green-500 text-3xl hover:text-green-700 cursor-pointer' /></Link>
                                    <label onClick={() => setDeleteMonth(month)} htmlFor="monthDelete">
                                        <RiDeleteBin6Fill className='text-red-500 text-3xl hover:text-red-700 cursor-pointer' />
                                    </label>
                                </td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
            {
                deleteMonth && <MonthDelete deleteMonth={deleteMonth} refetch={refetch} setDeleteMonth={setDeleteMonth} />
            }
        </div>
    );
};

export default ContentMonthShow;