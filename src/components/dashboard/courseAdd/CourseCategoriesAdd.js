import React from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import CourseCategoriesShow from './CourseCategoriesShow';

const CourseCategoriesAdd = () => {
    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    const onSubmit = async data => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/categoriesAdd`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
                // authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(output => {
                if (output.status === "success") {
                    toast.success('course categories add successfully')
                    reset();
                } else {
                    toast.error('Failed added to course categories')
                }
            })
    }
    return (
        <div>
            <div className='mx-5 p-4 bg-white my-10 rounded-xl'>
                <h2 className='text-2xl font-bold '>কোর্স ক্যাটাগরিস এড</h2>
            </div>
            <form className='mx-5 bg-white p-5 rounded-xl' onSubmit={handleSubmit(onSubmit)}>
                <div className="form-control w-full max-w-lg mt-5">
                    <label className="label">
                        <span className="label-text">কোর্স ক্যাটাগরিস</span>
                    </label>
                    <input type="text" placeholder="কোর্সের ক্যাটাগরিস" className="input input-bordered w-full max-w-lg" {...register("courseCategories", { required: true })} />
                </div>
                <input className='px-10 py-3 text-white bg-green-400 hover:bg-green-500 font-bold mt-10 rounded-lg cursor-pointer' type="submit" value="Categories Add" />
            </form>
            <div>
                <CourseCategoriesShow />
            </div>
        </div>
    );
};

export default CourseCategoriesAdd;