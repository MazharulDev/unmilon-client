import React from 'react';
import { useForm } from 'react-hook-form';
import { useQuery } from 'react-query';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';

const CourseCategoriesUpdate = () => {
    const navigate = useNavigate()
    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const { id } = useParams()
    const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/getCategoriesById/${id}`;
    const { data: courseCategorie, isLoading, refetch } = useQuery('courseCategorie', () => fetch(url, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));
    refetch()
    if (isLoading) {
        return <LoadingSpinner />
    }
    const { data } = courseCategorie;

    const onSubmit = async data => {
        const courseCategories = {
            courseCategories: data.courseCategories
        }
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/catagoriesUpdate/${id}`, {
            method: 'PATCH',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(courseCategories)
        })
            .then(res => res.json())
            .then(result => {
                if (result.status === "success") {
                    toast.success("Categories update successfully")
                    navigate("/dashboard/categoriesadd")
                }

            })
    }

    return (
        <div>
            <div className='mx-5 p-4 bg-white my-10 rounded-xl'>
                <h2 className='text-2xl font-bold '>কোর্স ক্যাটাগরিস আপডেট</h2>
            </div>
            <form className='mx-5 bg-white p-5 rounded-xl' onSubmit={handleSubmit(onSubmit)}>
                <div className="form-control w-full max-w-lg mt-5">
                    <label className="label">
                        <span className="label-text">কোর্স ক্যাটাগরিস</span>
                    </label>
                    <input type="text" placeholder="কোর্সের ক্যাটাগরিস" className="input input-bordered w-full max-w-lg" {...register("courseCategories", { required: true })} defaultValue={data?.courseCategories} />
                </div>
                <input className='px-10 py-3 text-white bg-green-400 hover:bg-green-500 font-bold mt-10 rounded-lg cursor-pointer' type="submit" value="Categories update" />
            </form>
        </div>
    );
};

export default CourseCategoriesUpdate;