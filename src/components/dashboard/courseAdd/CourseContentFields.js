import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { RiDeleteBin6Fill } from 'react-icons/ri';
import { useQuery } from 'react-query';
import { toast } from 'react-toastify';
import useCourseName from '../../../shared/hooks/useCourseName';
import useGetSubject from '../../../shared/hooks/useGetSubject';
import ContentDelete from '../../../shared/modal/ContentDelete';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';
const getYouTubeID = require('get-youtube-id');
const CourseContentFields = () => {
    // `http://localhost:5000/api/v1/course-content/single-content?courseName=${courseName}&month=${monthName}&subject=${subjectName}`
    const [names] = useCourseName()
    const [deleteContent, setDeleteContent] = useState(null)
    const [courseName, setCourseName] = useState()
    const [monthName, setMonthName] = useState()
    const [subjectName, setSubjectName] = useState()
    const [months, setMonths] = useState([])
    const [subjects, setSubjects] = useState([])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month?coursename=${courseName}`)
            .then(res => res.json())
            .then(result => setMonths(result.data))
    }, [courseName])
    useEffect(() => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/subject?coursename=${courseName}&month=${monthName}`)
            .then(res => res.json())
            .then(result => setSubjects(result.data))
    }, [courseName, monthName])


    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { data: postContent, isLoading, refetch } = useQuery('postContent', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/single-content?courseName=${courseName}&month=${monthName}&subject=${subjectName}`, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));

    if (isLoading) {
        return <LoadingSpinner />
    }
    refetch()
    const onSubmit = async data => {
        const videoId = getYouTubeID(data.videoLink)
        const slider = data.slide[0]
        const sliderFile = new FormData();
        sliderFile.append('coursePdf', slider)
        const sliderUrl = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/coursepdf`;
        fetch(sliderUrl, {
            method: 'POST',
            body: sliderFile
        })
            .then(res => res.json())
            .then(sliderUrlResult => {
                if (sliderUrlResult.error) {
                    return toast.error(sliderUrlResult.error)
                } else {
                    const sliderStore = sliderUrlResult.pdfURL
                    const pdf = data.pdf[0]
                    const pdfFile = new FormData();
                    pdfFile.append('coursePdf', pdf)
                    const pdfUrl = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/coursepdf`;
                    fetch(pdfUrl, {
                        method: 'POST',
                        body: pdfFile
                    })
                        .then(res => res.json())
                        .then(pdfUrlResult => {
                            if (pdfUrlResult.error) {
                                return toast.error(pdfUrlResult.error)
                            } else {
                                const pdfStore = pdfUrlResult.pdfURL
                                const quiz = data.quiz[0]
                                const quizFile = new FormData();
                                quizFile.append('quiz', quiz)
                                const quizUrl = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/quizfile`;
                                fetch(quizUrl, {
                                    method: 'POST',
                                    body: quizFile
                                })
                                    .then(res => res.json())
                                    .then(quizResult => {
                                        if (quizResult.error) {
                                            return toast.error(quizResult.error)
                                        } else {
                                            const quizStore = quizResult.quizURL
                                            const contentData = {
                                                courseName: courseName,
                                                month: monthName,
                                                subject: subjectName,
                                                videoName: data.videoName,
                                                videoLink: videoId,
                                                slideName: data.slideName,
                                                slideFile: sliderStore,
                                                pdfName: data.pdfFileName,
                                                pdfFile: pdfStore,
                                                quizName: data.quizFileName,
                                                quizFile: quizStore
                                            }
                                            fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content`, {
                                                method: 'POST',
                                                headers: {
                                                    'content-type': 'application/json',
                                                    // authorization: `Bearer ${localStorage.getItem('accessToken')}`
                                                },
                                                body: JSON.stringify(contentData)
                                            })
                                                .then(res => res.json())
                                                .then(output => {

                                                    if (output.status === "success") {
                                                        toast.success('course add successfully')
                                                        reset()
                                                    } else {
                                                        toast.error('Failed added to course')
                                                    }
                                                })
                                        }
                                    })
                            }
                        })
                }
            })
    }

    return (
        <div>
            <div className=''>
                <div className='mx-5 p-4 bg-white my-10 rounded-xl'>
                    <h2 className='text-2xl font-bold '>কোর্স কনটেন্ট এড</h2>
                </div>
                <div className='flex justify-start items-start'>
                    <div className='mx-5 bg-white p-5 rounded-xl'>
                        <div className=' mb-5'>
                            <select className="select select-bordered w-full " value={courseName} onChange={(e) => setCourseName(e.target.value)} >
                                <option disabled selected>কোর্সের নাম সিলেক্ট করুন</option>
                                {
                                    names?.data?.map(name =>
                                        <>
                                            <option value={name.title}>{name.title}</option>
                                        </>
                                    )
                                }
                            </select>
                        </div>
                        <div className=' mb-5 '>
                            <select className="select select-bordered w-full " value={monthName} onChange={(e) => setMonthName(e.target.value)}>
                                <option disabled selected>কোর্সের মাস সিলেক্ট করুন</option>
                                {
                                    months?.map(name =>
                                        <>
                                            <option value={name.month}>{name.month}</option>
                                        </>
                                    )
                                }
                            </select>
                        </div>
                        <div className=' mb-5 '>
                            <select className="select select-bordered w-full " value={subjectName} onChange={(e) => setSubjectName(e.target.value)}>
                                <option disabled selected>কোর্সের বিষয় সিলেক্ট করুন</option>
                                {
                                    subjects?.map(name =>
                                        <>
                                            <option value={name?.subject}>{name?.subject}</option>
                                        </>
                                    )
                                }
                            </select>
                        </div>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className='mt-3'>
                                {/* Video name  */}
                                <div className='mb-5  flex justify-between items-center gap-3'>
                                    <div className="form-control w-full max-w-xl">
                                        <label className="label">
                                            <span className="label-text">ভিডিওর নাম</span>
                                        </label>
                                        <input type="text" placeholder="ভিডিওর নাম " className="input input-bordered w-full" {...register("videoName", { required: true })} />
                                    </div>
                                    <div className="form-control w-full max-w-xl">
                                        <label className="label">
                                            <span className="label-text">ভিডিওর লিংক</span>
                                        </label>
                                        <input type="text" placeholder="ভিডিওর লিংক  " className="input input-bordered w-full" {...register("videoLink", { required: true })} />
                                    </div>
                                </div>

                                {/* slide  */}
                                <div className='mb-5  flex justify-between items-center gap-3'>
                                    <div className="form-control w-full max-w-xl">
                                        <label className="label">
                                            <span className="label-text">স্লাইডের নাম </span>
                                        </label>
                                        <input type="text" placeholder="স্লাইডের নাম  " className="input input-bordered w-full" {...register("slideName", { required: true })} />
                                    </div>
                                    <div className="form-control w-full max-w-xl">
                                        <label className="label">
                                            <span className="label-text">স্লাইড আপলোড</span>
                                        </label>
                                        <input type="file" className="file-input file-input-bordered w-full max-w-lg" accept="application/pdf" {...register("slide", { required: true })} />
                                    </div>
                                </div>
                                {/* pdf  */}
                                <div className='mb-5  flex justify-between items-center gap-3'>
                                    <div className="form-control w-full max-w-xl">
                                        <label className="label">
                                            <span className="label-text">পিডিএফ ফাইলের নাম  </span>
                                        </label>
                                        <input type="text" placeholder="পিডিএফ ফাইলের নাম  " className="input input-bordered w-full" {...register("pdfFileName", { required: true })} />
                                    </div>
                                    <div className="form-control w-full max-w-xl">
                                        <label className="label">
                                            <span className="label-text">পিডিএফ আপলোড</span>
                                        </label>
                                        <input type="file" className="file-input file-input-bordered w-full max-w-lg" accept="application/pdf" {...register("pdf", { required: true })} />
                                    </div>
                                </div>
                                {/* quiz  */}
                                <div className='mb-5  flex justify-between items-center gap-3'>
                                    <div className="form-control w-full max-w-xl">
                                        <label className="label">
                                            <span className="label-text">কুইজ ফাইলের নাম  </span>
                                        </label>
                                        <input type="text" placeholder="কুইজ ফাইলের নাম  " className="input input-bordered w-full" {...register("quizFileName", { required: true })} />
                                    </div>
                                    <div className="form-control w-full max-w-xl">
                                        <label className="label">
                                            <span className="label-text">কুইজ আপলোড</span>
                                        </label>
                                        <input type="file" className="file-input file-input-bordered w-full max-w-lg" accept="application/json" {...register("quiz", { required: true })} />
                                    </div>
                                </div>
                            </div>
                            <input className='px-10 py-3 bg-green-400 hover:bg-green-500 font-bold mt-5 rounded-lg cursor-pointer' type="submit" />
                        </form>
                    </div>
                    <div>
                        {
                            postContent?.data?.length >= 1 &&
                            <table className="table  mx-5">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Video Name</th>
                                        <th>Action</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        postContent?.data?.map((content, index) =>
                                            <tr key={content._id}>
                                                <th>{index + 1}</th>
                                                <td>{content.videoName}</td>
                                                <td className='flex justify-start items-center gap-4 py-8'>
                                                    {/* <Link to={`/dashboard/categoriesupdate/${content._id}`}><BiEdit className='text-green-500 text-3xl hover:text-green-700 cursor-pointer' /></Link> */}
                                                    <label onClick={() => setDeleteContent(content)} htmlFor="contentDelete">
                                                        <RiDeleteBin6Fill className='text-red-500 text-3xl hover:text-red-700 cursor-pointer' />
                                                    </label>
                                                </td>
                                            </tr>
                                        )
                                    }
                                </tbody>
                            </table>
                        }
                    </div>
                </div>

            </div>
            {
                deleteContent && <ContentDelete deleteContent={deleteContent} setDeleteContent={setDeleteContent} refetch={refetch} />
            }

        </div>
    );
};

export default CourseContentFields;