import React from 'react';
import { useForm } from 'react-hook-form';
import { useQuery } from 'react-query';
import { toast } from "react-toastify"
import { useNavigate, useParams } from 'react-router-dom';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';

const ContentMonthUpdate = () => {
    const navigate = useNavigate()
    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const { id } = useParams()
    const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month/${id}`;
    const { data: months, isLoading, refetch } = useQuery('monthUpdate', () => fetch(url, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));
    refetch()
    if (isLoading) {
        return <LoadingSpinner />
    }
    const { data } = months;

    const onSubmit = async data => {
        const month = {
            month: data.month
        }
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month/${id}`, {
            method: 'PATCH',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(month)
        })
            .then(res => res.json())
            .then(result => {
                if (result.status === "success") {
                    toast.success("Month update successfully")
                    navigate("/dashboard/month-add")
                } else {
                    toast.error("Month update failed")
                }

            })
    }
    return (
        <div>
            <div className='mx-5 p-4 bg-white my-10 rounded-xl'>
                <h2 className='text-2xl font-bold '>মাস আপডেট</h2>
            </div>
            <form className='mx-5 bg-white p-5 rounded-xl' onSubmit={handleSubmit(onSubmit)}>
                <div className="form-control w-full max-w-lg mt-5">
                    <label className="label">
                        <span className="label-text">কনটেন্ট মাস</span>
                    </label>
                    <input type="text" placeholder="মাস" className="input input-bordered w-full max-w-lg" {...register("month", { required: true })} defaultValue={data?.month} />
                </div>
                <input className='px-10 py-3 text-white bg-green-400 hover:bg-green-500 font-bold mt-10 rounded-lg cursor-pointer' type="submit" value="Month update" />
            </form>
        </div>
    );
};

export default ContentMonthUpdate;