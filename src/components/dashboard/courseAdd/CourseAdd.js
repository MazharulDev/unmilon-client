import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import DescriptionForCourseAdd from '../DescriptionForCourseAdd';
import CourseCategories from './CourseCategories';
// import CourseContentFields from './CourseContentFields';
// import QuizDataCsv from './QuizDataCsv.tsx';



const getYouTubeID = require('get-youtube-id');

const CourseAdd = () => {

    const [value, setValue] = useState('');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    const onSubmit = async data => {

        const image = data.img[0];
        const formData = new FormData();
        formData.append('courseImg', image);
        const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/coursethum`;
        const youtubeId = getYouTubeID(data.courseIntro)

        fetch(url, {
            method: 'POST',
            body: formData

        })
            .then(res => res.json())
            .then(result => {
                if (result.error) {
                    return toast.error(result.error)
                } else {
                    const img = result.imgURL;
                    const courseData = {
                        courseCategories: data.courseCategories,
                        title: data.title,
                        offer: data.offer,
                        status: data.status,
                        img: img,
                        courseIntro: youtubeId,
                        price: data.price,
                        description: value

                    }
                    fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses`, {
                        method: 'POST',
                        headers: {
                            'content-type': 'application/json',
                            // authorization: `Bearer ${localStorage.getItem('accessToken')}`
                        },
                        body: JSON.stringify(courseData)
                    })
                        .then(res => res.json())
                        .then(output => {
                            console.log(output);
                            if (output.status === "success") {
                                toast.success('course add successfully')
                                reset();
                            } else {
                                toast.error('Failed added to course')
                            }
                        })
                }

            })



    }

    return (
        <div>
            <div className='mx-5 p-4 bg-white my-10 rounded-xl'>
                <h2 className='text-2xl font-bold '>কোর্স এড করুন</h2>
            </div>
            <form className='mx-5 bg-white p-5 rounded-xl' onSubmit={handleSubmit(onSubmit)}>
                <p className='text-lg font-bold mb-5'>কোর্স ইনফো</p>
                <div className="form-control w-full max-w-lg">
                    <label className="label">
                        <span className="label-text">কোর্স ক্যাটাগরি</span>

                    </label>
                    <CourseCategories register={register} />
                </div>
                <div className="form-control w-full max-w-xl mt-5">
                    <label className="label">
                        <span className="label-text">কোর্স টাইটেল</span>
                    </label>
                    <input type="text" placeholder="কোর্স টাইটেল" className="input input-bordered w-full max-w-lg" {...register("title", { required: true })} />
                </div>
                <div className="form-control w-full max-w-lg mt-5">
                    <label className="label">
                        <span className="label-text">কোর্স স্ট্যাটাস</span>
                    </label>
                    <input type="text" placeholder="ex. রেজিস্ট্রেশন চলছে" className="input input-bordered w-full max-w-lg" {...register("status", { required: true })} />
                </div>

                <div className="form-control w-full max-w-lg">
                    <label className="label">
                        <span className="label-text">অফার</span>

                    </label>
                    <select className="select select-bordered" {...register("offer", { required: true })}>
                        <option disabled selected>অফার</option>
                        <option value="Running">অফার চলতেছে</option>
                        <option value="Ended">অফার বন্ধ </option>
                    </select>
                </div>
                <div className="form-control w-full max-w-lg mt-5">
                    <label className="label">
                        <span className="label-text">কোর্স প্রাইস</span>
                    </label>
                    <input type="number" placeholder="কোর্সের প্রাইস" className="input input-bordered w-full max-w-lg" {...register("price", { required: true })} />
                </div>

                <div className="form-control w-full max-w-lg mt-5">
                    <label className="label">
                        <span className="label-text">কোর্স Intro ভিডিও</span>
                    </label>
                    <input type="url" placeholder="কোর্স Intro ভিডিও লিংক" className="input input-bordered w-full max-w-lg" {...register("courseIntro", { required: true })} />
                </div>

                <div className="form-control w-full max-w-lg mt-5">
                    <label className="label">
                        <span className="label-text">কোর্সের thumbnail ইমেজ</span>
                    </label>
                    <input type="file" className="file-input file-input-bordered w-full max-w-lg" accept="image/*" {...register("img", { required: true })} />
                </div>

                <div className='mt-5 max-w-lg'>
                    <label className="label">
                        <p className='label-text'>কোর্সের ডেসক্রিপশন</p>
                    </label>
                    <DescriptionForCourseAdd setValue={setValue} value={value} />
                </div>

                {/* <div className='mt-5 max-w-lg'>
                    <p className='text-lg font-bold my-8'>কোর্স কনটেন্ট</p>
                    <div >

                        <CourseContentFields
                            setInputFields={setInputFields}
                            inputFields={inputFields}
                        />

                    </div>


                </div> */}
                {/* <div className='mt-5 mx-w-lg'>
                    <QuizDataCsv />
                </div> */}

                <input className='px-10 py-3 bg-green-400 hover:bg-green-500 font-bold mt-10 rounded-lg cursor-pointer' type="submit" />

            </form>

        </div>
    );
};

export default CourseAdd;