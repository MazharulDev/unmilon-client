import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import useCourseName from '../../../shared/hooks/useCourseName';
import ContentMonthShow from './ContentMonthShow';

const ContentMonthAdd = () => {
    const [names] = useCourseName()
    const [courseName, setCourseName] = useState()

    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    const onSubmit = async data => {
        const monthAdd = {
            courseName: courseName,
            month: data.month
        }
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
                // authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify(monthAdd)
        })
            .then(res => res.json())
            .then(output => {
                if (output.status === "success") {
                    toast.success('Month add successfully')
                    reset();
                } else {
                    toast.error('Failed added to Month')
                }
            })
    }
    return (
        <div>
            <div className='mx-5 p-4 bg-white my-10 rounded-xl'>
                <h2 className='text-2xl font-bold '>কনটেন্ট মাস এড</h2>
            </div>
            <div className='mx-5 bg-white p-5 rounded-xl'>
                <div className=' mb-5 max-w-lg'>
                    <label className="label">
                        <span className="label-text">কোর্স </span>
                    </label>
                    <select className="select select-bordered w-full " value={courseName} onChange={(e) => setCourseName(e.target.value)} >
                        <option disabled selected>কোর্সের নাম সিলেক্ট করুন</option>
                        {
                            names?.data?.map(name =>
                                <>
                                    <option value={name.title}>{name.title}</option>
                                </>
                            )
                        }
                    </select>
                </div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-control w-full max-w-lg mt-5">
                        <label className="label">
                            <span className="label-text">কনটেন্ট মাস</span>
                        </label>
                        <input type="text" placeholder="কনটেন্ট মাস" className="input input-bordered w-full max-w-lg" {...register("month", { required: true })} />
                    </div>
                    <input className='px-10 py-3 text-white bg-green-400 hover:bg-green-500 font-bold mt-10 rounded-lg cursor-pointer' type="submit" value="Month Add" />
                </form>
            </div>
            <div>
                <ContentMonthShow courseName={courseName} />
            </div>
        </div>
    );
};

export default ContentMonthAdd;