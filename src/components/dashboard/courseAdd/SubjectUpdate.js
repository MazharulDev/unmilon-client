import React from 'react';
import { useForm } from 'react-hook-form';
import { useQuery } from 'react-query';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';

const SubjectUpdate = () => {
    const navigate = useNavigate()
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { id } = useParams()
    const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/subject/${id}`;
    const { data: subject, isLoading, refetch } = useQuery('subjectUpdate', () => fetch(url, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));
    refetch()
    if (isLoading) {
        return <LoadingSpinner />
    }
    const { data } = subject;
    const onSubmit = async data => {
        const subject = {
            subject: data.subject
        }
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/subject/${id}`, {
            method: 'PATCH',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(subject)
        })
            .then(res => res.json())
            .then(result => {
                if (result.status === "success") {
                    toast.success("Month update successfully")
                    navigate("/dashboard/subject-add")
                } else {
                    toast.error("Month update failed")
                }

            })
    }
    return (
        <div>
            <div className='mx-5 p-4 bg-white my-10 rounded-xl'>
                <h2 className='text-2xl font-bold '>সাবজেক্ট আপডেট</h2>
            </div>
            <form className='mx-5 bg-white p-5 rounded-xl' onSubmit={handleSubmit(onSubmit)}>
                <div className="form-control w-full max-w-lg mt-5">
                    <label className="label">
                        <span className="label-text">সাবজেক্ট</span>
                    </label>
                    <input type="text" placeholder="সাবজেক্ট" className="input input-bordered w-full max-w-lg" {...register("subject", { required: true })} defaultValue={data?.subject} />
                </div>
                <input className='px-10 py-3 text-white bg-green-400 hover:bg-green-500 font-bold mt-10 rounded-lg cursor-pointer' type="submit" value="Subject update" />
            </form>
        </div>
    );
};

export default SubjectUpdate;