import React, { useState } from 'react';
import { BiEdit } from 'react-icons/bi';
import { RiDeleteBin6Fill } from 'react-icons/ri';
import { useQuery } from 'react-query';
import { Link } from "react-router-dom"
import CategoriesDelete from '../../../shared/modal/CategoriesDelete';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';

const CourseCategoriesShow = () => {
    const [deleteCategories, setDeleteCategories] = useState(null)
    const { data: courseAddCategories, isLoading, refetch } = useQuery('courseAddCategories', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/categoriesAdd`, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));

    if (isLoading) {
        return <LoadingSpinner />
    }
    refetch()
    return (
        <div className='my-10'>
            <table className="table w-2/4 mx-5">
                <thead>
                    <tr>
                        <th></th>
                        <th>Categories</th>
                        <th>Action</th>


                    </tr>
                </thead>
                <tbody>
                    {
                        courseAddCategories?.data?.map((categorie, index) =>
                            <tr key={categorie._id}>
                                <th>{index + 1}</th>
                                <td>{categorie.courseCategories}</td>
                                <td className='flex justify-start items-center gap-4 py-8'>
                                    <Link to={`/dashboard/categoriesupdate/${categorie._id}`}><BiEdit className='text-green-500 text-3xl hover:text-green-700 cursor-pointer' /></Link>
                                    <label onClick={() => setDeleteCategories(categorie)} htmlFor="categoriesDelete">
                                        <RiDeleteBin6Fill className='text-red-500 text-3xl hover:text-red-700 cursor-pointer' />
                                    </label>
                                </td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
            {
                deleteCategories && <CategoriesDelete deleteCategories={deleteCategories} refetch={refetch} setDeleteCategories={setDeleteCategories} />
            }
        </div>
    );
};

export default CourseCategoriesShow;