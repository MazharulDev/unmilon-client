import React from 'react';
import { useQuery } from 'react-query';
import LoadingSpinner from '../../../shared/spinner/LoadingSpinner';

const CourseCategories = ({ register }) => {
    const { data: categories, isLoading, refetch } = useQuery('courseAddCategories', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/categoriesAdd`, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));

    if (isLoading) {
        return <LoadingSpinner />
    }
    refetch()

    // http://localhost:5000/api/v1/courses/categoriesAdd
    return (
        <select className="select select-bordered" {...register("courseCategories", { required: true })}>
            <option disabled selected>কোর্স ক্যাটাগরি</option>
            {
                categories?.data?.map(categori =>

                    <>

                        <option value={categori.courseCategories}>{categori.courseCategories}</option>
                    </>

                )
            }
        </select>
    );
};

export default CourseCategories;