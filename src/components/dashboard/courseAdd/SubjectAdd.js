import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import useCourseName from '../../../shared/hooks/useCourseName';
import SubjectShow from './SubjectShow';

const SubjectAdd = () => {
    const [names] = useCourseName()
    const [courseName, setCourseName] = useState()
    const [courseMonths, setCourseMonths] = useState()
    const [months, setMonths] = useState([])
    useEffect(() => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month?coursename=${courseName}`)
            .then(res => res.json())
            .then(result => setMonths(result.data))
    }, [courseName])
    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const onSubmit = async data => {
        const subjectSent = {
            courseName: courseName,
            month: courseMonths,
            subject: data.subject
        }
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/subject`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
                // authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify(subjectSent)
        })
            .then(res => res.json())
            .then(output => {
                if (output.status === "success") {
                    toast.success('Subject add successfully')
                    reset();
                } else {
                    toast.error('Failed added to Subject')
                }
            })
    }
    return (
        <div>
            <div className='mx-5 p-4 bg-white my-10 rounded-xl'>
                <h2 className='text-2xl font-bold '>সাবজেক্ট এড করুন</h2>
            </div>
            <div className='mx-5 bg-white p-5 rounded-xl'>
                <div className=' mb-5 max-w-lg'>
                    <label className="label">
                        <span className="label-text">কোর্সের নাম</span>
                    </label>
                    <select className="select select-bordered w-full " value={courseName} onChange={(e) => setCourseName(e.target.value)} >
                        <option disabled selected>কোর্সের নাম সিলেক্ট করুন</option>
                        {
                            names?.data?.map(name =>
                                <>
                                    <option value={name.title}>{name.title}</option>
                                </>
                            )
                        }
                    </select>


                </div>

                <div className='mb-5 max-w-lg'>
                    <label className="label">
                        <span className="label-text">কোর্সের মাস</span>
                    </label>
                    <select className="select select-bordered w-full " value={courseMonths} onChange={(e) => setCourseMonths(e.target.value)} >
                        <option disabled selected>কোর্সের মাস সিলেক্ট করুন</option>
                        {
                            months?.map(name =>
                                <>
                                    <option value={name.month}>{name.month}</option>
                                </>
                            )
                        }
                    </select>
                </div>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-control w-full max-w-lg mt-5">
                        <label className="label">
                            <span className="label-text">সাবজেক্ট এড</span>
                        </label>
                        <input type="text" placeholder="সাবজেক্ট" className="input input-bordered w-full max-w-lg" {...register("subject", { required: true })} />
                    </div>
                    <input className='px-10 py-3 text-white bg-green-400 hover:bg-green-500 font-bold mt-10 rounded-lg cursor-pointer' type="submit" value="Month Add" />
                </form>
            </div>
            <div>
                <SubjectShow courseName={courseName} courseMonths={courseMonths} />
            </div>
        </div>
    );
};

export default SubjectAdd;