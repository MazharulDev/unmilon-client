import React from 'react';
import { HiUserGroup } from 'react-icons/hi'
import { FaChalkboardTeacher } from 'react-icons/fa'
import { AiOutlineDollarCircle, AiOutlineFileProtect } from 'react-icons/ai';
import DashboardHomeChart from './DashboardHomeChart';
import DashboardHomePicChart from './DashboardHomePicChart';
import DashboardTable from './DashboardTable';

const DashboardHome = () => {
    return (
        <div className='mx-5 my-4'>
            <h2 className='text-2xl font-bold text-blue-600'>Hi, Welcome back !</h2>
            <h3 className='text-xl font-bold'>Unmilon Dashboard,</h3>
            <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-8 mt-10'>
                <div className='bg-white shadow-lg p-4  rounded-md'>
                    <div className='flex justify-between items-center pb-5'>
                        <div className='w-11 text-white'>
                            <div className=' bg-blue-700 rounded-xl p-3'>
                                <HiUserGroup className='text-xl' />
                            </div>
                        </div>
                        <div>
                            <h4 className='text-lg font-bold'>Total Students</h4>
                            <p className='text-2xl font-bold text-end'>1,250</p>
                        </div>
                    </div>
                    <progress className="progress progress-primary w-full" value="40" max="100"></progress>
                </div>
                <div className='bg-white shadow-lg p-4  rounded-md'>
                    <div className='flex justify-between items-center pb-5'>
                        <div className='w-11 text-white'>
                            <div className=' bg-red-500 rounded-xl p-3'>
                                <FaChalkboardTeacher className='text-xl' />
                            </div>
                        </div>
                        <div>
                            <h4 className='text-lg font-bold'>Total Teachers</h4>
                            <p className='text-2xl font-bold text-end'>21</p>
                        </div>
                    </div>
                    <progress className="progress progress-secondary w-full" value="32" max="100"></progress>
                </div>
                <div className='bg-white shadow-lg p-4  rounded-md'>
                    <div className='flex justify-between items-center pb-5'>
                        <div className='w-11 text-white'>
                            <div className=' bg-green-400 rounded-xl p-3'>
                                <AiOutlineFileProtect className='text-xl' />
                            </div>
                        </div>
                        <div>
                            <h4 className='text-lg font-bold'>Total Courses</h4>
                            <p className='text-2xl font-bold text-end'>15</p>
                        </div>
                    </div>
                    <progress className="progress progress-success w-full" value="75" max="100"></progress>
                </div>
                <div className='bg-white shadow-lg p-4  rounded-md'>
                    <div className='flex justify-between items-center pb-5'>
                        <div className='w-11 text-white'>
                            <div className=' bg-yellow-500 rounded-xl p-3'>
                                <AiOutlineDollarCircle className='text-xl' />
                            </div>
                        </div>
                        <div>
                            <h4 className='text-lg font-bold'>Total Earnings</h4>
                            <p className='text-2xl font-bold text-end'>22,2500</p>
                        </div>
                    </div>
                    <progress className="progress progress-warning w-full" value="80" max="100"></progress>
                </div>
            </div>

            <div className='grid grid-cols-3 gap-4 mt-5'>
                <div className='bg-white py-4 col-span-2 shadow-lg rounded-lg'>
                    <DashboardHomeChart />
                </div>
                <div className='bg-white py-4 flex justify-center items-center shadow-lg rounded-lg'>
                    <DashboardHomePicChart />
                </div>
            </div>
            <div className='mt-12 w-full'>
                <DashboardTable />
            </div>

        </div>
    );
};

export default DashboardHome;