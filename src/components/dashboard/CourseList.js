import React from 'react';
import { useQuery } from 'react-query';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';
import CourseListContent from './CourseListContent';

const CourseList = () => {
    const { data: courses, isLoading, refetch } = useQuery('courses', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses`, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));
    refetch()
    if (isLoading) {
        return <LoadingSpinner />
    }
    return (
        <div>
            <div className='flex justify-center'>
                <h2 className='text-center text-4xl py-5 inline-block border-b-4 border-yellow-400 font-bold mb-5'>Course List</h2>
            </div>
            <div className="overflow-x-auto px-2">
                <table className="table w-full">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Course thumbnail</th>
                            <th>Course Categories</th>
                            <th>Course Title</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        {
                            courses?.data?.map((course, index) =>
                                <CourseListContent key={index} index={index} course={course} refetch={refetch} />
                            )
                        }
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default CourseList;