import { signOut } from 'firebase/auth';
import React, { useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { AiOutlineMenuUnfold } from 'react-icons/ai'
import { useQuery } from 'react-query';
import { Link, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import auth from '../../firebase.init';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';

const DashboardHeader = () => {
    const navigate = useNavigate()
    const [user, loading, error] = useAuthState(auth);

    const handleSignOut = () => {
        signOut(auth).then((user) => {
            if (!user) {
                toast("Sign Out successfully");
                navigate("/login")
            }
        });
    }
    const { data: userInfo, isLoading, refetch } = useQuery('userInfo', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/userprofile?email=${user?.email}`, {
        method: 'GET',
    })
        .then(res => res.json()))


    refetch();
    const info = userInfo?.data[0]
    if (loading || isLoading) {
        return <LoadingSpinner />
    }
    return (
        <div className='sticky top-0 z-30 mb-2'>
            <div className="navbar bg-base-100 ">
                <div className="flex-1">
                    <div className='flex justify-start items-center gap-3'>
                        <label htmlFor="my-drawer-2" className=" drawer-button lg:hidden text-xl"><AiOutlineMenuUnfold /></label>
                        <a className="btn btn-ghost normal-case text-xl">Unmilon Studio</a>
                    </div>
                </div>
                <div className="flex-none">
                    <div className="dropdown dropdown-end">
                        <label tabIndex={0} className="btn btn-ghost btn-circle">
                            <div className="indicator">
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" /></svg>
                                <span className="badge badge-sm indicator-item">8</span>
                            </div>
                        </label>
                        <div tabIndex={0} className="mt-3 card card-compact dropdown-content w-52 bg-base-100 shadow">
                            <div className="card-body">
                                <span className="font-bold text-lg">8 Items</span>
                                <span className="text-info">Subtotal: $999</span>
                                <div className="card-actions">
                                    <button className="btn btn-primary btn-block">View cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="dropdown dropdown-end">
                        {
                            user &&
                            <div className="dropdown dropdown-end">
                                <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
                                    <div className="w-10 rounded-full">
                                        {info?.imgURL ? <img src={info?.imgURL} alt='' /> : <div className='text-3xl h-full text-white bg-primary flex justify-center items-center'>{user.displayName ? user?.displayName?.substring(0, 1) : 'U'}</div>}
                                    </div>
                                </label>
                                <ul tabIndex={0} className="mt-3 p-2 shadow menu menu-compact dropdown-content bg-base-100 rounded-box w-52">
                                    <li>
                                        <Link to="/profile" className="justify-between">
                                            প্রোফাইল
                                        </Link>
                                    </li>
                                    <li><button onClick={handleSignOut}>লগআউট</button></li>
                                </ul>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DashboardHeader;