import React from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const modules = {
    toolbar: [
        // [{ Header: [1, 2, 3, 4, 5, 6] }],
        [{ font: [] }],
        [{ size: [] }],
        ["bold", "italic", "underline", "strike", "blockquote"],
        [
            { list: "ordered" },
            { list: "bullet" },
            { indent: "-1" },
            { indent: "+1" }
        ],
        ["link", "image", "video"],
        [{ 'color': [] }, { 'background': [] }],
        [{ 'align': [] }],

        ['clean']
    ]
}

const DescriptionForCourseAdd = ({ setValue, value }) => {

    return (
        <div>
            <ReactQuill
                theme="snow"
                value={value}
                onChange={setValue}
                modules={modules}
                placeholder='Write course description...'

            />
            {/* <div
                dangerouslySetInnerHTML={{ __html: value }}
            /> */}
        </div>
    );
};

export default DescriptionForCourseAdd;