import React from 'react';
import { Link, Outlet } from 'react-router-dom';
import DashboardHeader from './DashboardHeader';
import { ImExit } from 'react-icons/im'
import { useAuthState } from 'react-firebase-hooks/auth';
import auth from '../../firebase.init';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';
import { useQuery } from 'react-query';

const Dashboard = () => {
    const [user, loading, error] = useAuthState(auth);
    const { data: userInfo, isLoading, refetch } = useQuery('userInfo', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/userprofile?email=${user?.email}`, {
        method: 'GET',
    })
        .then(res => res.json()))

    if (loading || isLoading) {
        return <LoadingSpinner />
    }
    refetch();
    const info = userInfo?.data[0]
    return (
        <div className=''>
            <div className="drawer drawer-mobile">
                <input id="my-drawer-2" type="checkbox" className="drawer-toggle" />
                <div className="drawer-content bg-slate-200">
                    <div className=''>
                        <DashboardHeader />
                    </div>
                    {/* <!-- Page content here --> */}

                    <Outlet />
                </div>
                <div className="drawer-side">
                    <label htmlFor="my-drawer-2" className="drawer-overlay"></label>
                    <ul className="menu p-4 w-56 shadow-lg bg-white lg:bg-transparent">
                        <Link to="/" className='flex justify-start items-center text-red-500 gap-3 hover:text-red-700'>
                            <ImExit />
                            <p className='text-xl  font-bold'>Exit</p>
                        </Link>
                        <h2 className='my-5 text-center text-xl font-bold'>Unmilon Studio</h2>
                        <div className='flex justify-center items-center'>
                            <div className="avatar">
                                <div className="w-16 rounded-xl">
                                    {info?.imgURL ? <img src={info?.imgURL} alt='' /> : <div className='text-3xl h-full text-white bg-primary flex justify-center items-center'>{user.displayName ? user?.displayName?.substring(0, 1) : 'U'}</div>}
                                </div>
                            </div>
                        </div>
                        <div className='text-center mt-2 mb-3'>
                            <h2 className='text-lg'>{user?.displayName}</h2>
                            <p className='text-sm'>Admin</p>
                        </div>
                        {/* <!-- Sidebar content here --> */}
                        <p className='text-slate-500 my-2'>OVERVIEW</p>
                        <li><Link to="/dashboard">Overview</Link></li>
                        <p className='text-slate-500 my-2'>PAGES</p>
                        <li><Link to="/dashboard/user-managment">User Managment</Link></li>
                        <li><Link to="/dashboard/courselist">Course List</Link></li>
                        <div className="collapse border border-base-300">
                            <input type="checkbox" className="peer" />
                            <div className="collapse-title ">
                                Course add
                            </div>
                            <div className="collapse-content">
                                <li><Link to="/dashboard/courseadd">Course Add</Link></li>
                                <li><Link to="/dashboard/contents">Course contents</Link></li>
                                <li><Link to="/dashboard/categoriesadd">Categories add</Link></li>
                                <li><Link to="/dashboard/month-add">Month Add</Link></li>
                                <li><Link to="/dashboard/subject-add">Subject Add</Link></li>
                            </div>
                        </div>

                    </ul>

                </div>
            </div>

        </div>
    );
};

export default Dashboard;