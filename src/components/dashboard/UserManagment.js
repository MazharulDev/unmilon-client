import React from 'react';
import { useQuery } from 'react-query';
import LoadingSpinner from '../../shared/spinner/LoadingSpinner';
import User from './User';

const UserManagment = () => {
    const { data: users, isLoading, refetch } = useQuery('users', () => fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users`, {
        method: 'GET',
        // headers: {
        //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // }
    })
        .then(res => res.json()));
    refetch()
    if (isLoading) {
        return <LoadingSpinner />
    }
    return (
        <div>
            <div className='flex justify-center'>
                <h2 className='text-center text-4xl py-5 inline-block border-b-4 border-yellow-400 font-bold mb-5'>User Management</h2>
            </div>
            <div className="overflow-x-auto px-2">
                <table className="table w-full">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Email</th>
                            <th>Make Admin</th>
                            <th>Make Teacher</th>
                            <th>Remove user</th>

                        </tr>
                    </thead>
                    <tbody>
                        {
                            users?.data?.map((user, index) =>
                                <User key={index} index={index} user={user} refetch={refetch} />
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* {userDelete && <UserDeleteModal user={userDelete} refetch={refetch} setUserDelete={setUserDelete} />} */}
        </div>
    );
};

export default UserManagment;