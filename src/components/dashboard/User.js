import React, { useState } from 'react';
import { toast } from 'react-toastify';
import UserDelete from '../../shared/modal/UserDelete';

const User = ({ user, index, refetch }) => {
    const [userDelete, setUserDelete] = useState(null);

    const handleMakeAdmin = () => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/makeadmin/${user?.email}`, {
            method: 'PUT',
            // headers: {
            //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
            // }
        })
            .then(res => {
                if (res.status === 'Failed') {
                    toast.error('Failed to Make an admin');
                }
                return res.json()
            }
            )
            .then(data => {
                if (data?.data?.modifiedCount > 0) {
                    refetch();
                    toast.success('Make an admin successfully')
                }
            })
    }
    const handleMakeTeacher = () => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/maketeacher/${user?.email}`, {
            method: 'PUT',
            // headers: {
            //     authorization: `Bearer ${localStorage.getItem('accessToken')}`
            // }
        })
            .then(res => {
                if (res.status === 'Failed') {
                    toast.error('Failed to Make a teacher');
                }
                return res.json()
            }
            )
            .then(data => {
                if (data?.data?.modifiedCount > 0) {
                    refetch();
                    toast.success('Make a teacher successfully')
                }
            })
    }

    return (
        <>
            <tr>
                <th>{index + 1}</th>
                <td>{user.email}</td>
                <td>{user.role !== "admin" ? <button onClick={handleMakeAdmin} className="btn btn-xs btn-success hover:bg-green-500">Make Admin</button> : <button disabled className="btn btn-xs btn-success hover:bg-green-500">Already Admin</button>}</td>

                {/* <button onClick={handleMakeAdmin} className="btn btn-xs btn-success hover:bg-green-500">Admin</button> */}
                <td>{user.role !== "teacher" ? <button onClick={handleMakeTeacher} className="btn btn-xs btn-success hover:bg-green-500">Make Teacher</button> : <button disabled className="btn btn-xs btn-success hover:bg-green-500">Already Teacher</button>}</td>
                <td>
                    <label onClick={() => setUserDelete(user)} htmlFor="userDelete">
                        <p className="btn btn-xs btn-error hover:bg-red-500">Remove</p>
                    </label>
                </td>
            </tr>
            {
                userDelete && <UserDelete userDelete={userDelete} refetch={refetch} setUserDelete={setUserDelete} />
            }
        </>
    );
};

export default User;