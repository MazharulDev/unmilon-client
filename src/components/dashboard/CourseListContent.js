import React, { useState } from 'react';
import { BiEdit } from 'react-icons/bi';
import { RiDeleteBin6Fill } from "react-icons/ri"
import { toast } from 'react-toastify';
import CourseDelete from '../../shared/modal/CourseDelete';

const CourseListContent = ({ course, index, refetch }) => {
    const [courseDelete, setCourseDelete] = useState(null)
    return (
        <tr>
            <th>{index + 1}</th>
            <td><img width={100} src={course.img} alt="" /></td>
            <td>{course.courseCategories}</td>
            <td>{course.title}</td>
            <td className='flex justify-start items-center gap-4 py-8'>
                <BiEdit className='text-green-500 text-3xl hover:text-green-700 cursor-pointer' />
                <label onClick={() => setCourseDelete(course)} htmlFor="courseDelete">
                    <RiDeleteBin6Fill className='text-red-500 text-3xl hover:text-red-700 cursor-pointer' />
                </label>
            </td>
            {
                courseDelete && <CourseDelete courseDelete={courseDelete} refetch={refetch} setCourseDelete={setCourseDelete} />
            }
        </tr>
    );
};

export default CourseListContent;