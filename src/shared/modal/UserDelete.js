import React from 'react';
import { toast } from 'react-toastify';


const UserDelete = ({ userDelete, refetch, setUserDelete }) => {
    const handleUserDelete = id => {
        const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/users/${id}`
        fetch(url, {
            method: 'DELETE',
            // headers: {
            //     'authorization': `Bearer ${localStorage.getItem('accessToken')}`
            // }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === "success") {
                    toast.success('User delete successfully')
                    refetch()
                }

            })
    }
    const { _id, email } = userDelete;
    return (
        <div>

            <input type="checkbox" id="userDelete" className="modal-toggle" />
            <div className="modal modal-bottom sm:modal-middle">
                <div className="modal-box">
                    <h3 className="font-bold text-lg ">Are you sure you want to delete <span className='text-red-600'>{email}</span> </h3>

                    <div className="modal-action">
                        <label onClick={() => handleUserDelete(_id)} className='btn btn-xs btn-error'>Yes</label>
                        <label htmlFor="userDelete" className="btn btn-xs">NO</label>
                    </div>
                </div>
            </div>
        </div >
    );
};

export default UserDelete;