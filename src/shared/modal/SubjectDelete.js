import React from 'react';
import { toast } from 'react-toastify';

const SubjectDelete = ({ deleteSubject, setDeleteSubject, refetch }) => {
    const handleDelete = id => {
        const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/subject/${id}`
        fetch(url, {
            method: 'DELETE',
            // headers: {
            //     'authorization': `Bearer ${localStorage.getItem('accessToken')}`
            // }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === "success") {
                    toast.success('Month delete successfully')
                    setDeleteSubject(null)
                    refetch()
                }

            })
    }
    const { subject, _id } = deleteSubject;
    return (
        <div>
            <input type="checkbox" id="monthDelete" className="modal-toggle" />
            <div className="modal modal-bottom sm:modal-middle">
                <div className="modal-box">
                    <h3 className="font-bold text-lg text-red-600">Are you sure you want to delete <span className='text-red-500'>{subject}</span> </h3>

                    <div className="modal-action">
                        <label onClick={() => handleDelete(_id)} className='btn btn-xs btn-error'>Yes</label>
                        <label htmlFor="monthDelete" className="btn btn-xs">NO</label>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SubjectDelete;