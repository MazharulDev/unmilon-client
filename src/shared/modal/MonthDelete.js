import React from 'react';
import { toast } from 'react-toastify';

const MonthDelete = ({ deleteMonth, setDeleteMonth, refetch }) => {
    const handleDelete = id => {
        const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month/${id}`
        fetch(url, {
            method: 'DELETE',
            // headers: {
            //     'authorization': `Bearer ${localStorage.getItem('accessToken')}`
            // }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === "success") {
                    toast.success('Month delete successfully')
                    setDeleteMonth(null)
                    refetch()
                }

            })
    }
    const { month, _id } = deleteMonth;
    return (
        <div>

            <input type="checkbox" id="monthDelete" className="modal-toggle" />
            <div className="modal modal-bottom sm:modal-middle">
                <div className="modal-box">
                    <h3 className="font-bold text-lg text-red-600">Are you sure you want to delete <span className='text-red-500'>{month}</span> </h3>

                    <div className="modal-action">
                        <label onClick={() => handleDelete(_id)} className='btn btn-xs btn-error'>Yes</label>
                        <label htmlFor="monthDelete" className="btn btn-xs">NO</label>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MonthDelete;