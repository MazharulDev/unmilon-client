import React from 'react';
import { toast } from 'react-toastify';

const CategoriesDelete = ({ deleteCategories, refetch, setDeleteCategories }) => {
    const handleDelete = id => {
        const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/courses/categoriesdelete/${id}`
        fetch(url, {
            method: 'DELETE',
            // headers: {
            //     'authorization': `Bearer ${localStorage.getItem('accessToken')}`
            // }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === "success") {
                    toast.success('Categories delete successfully')
                    setDeleteCategories(null)
                    refetch()
                }

            })
    }
    const { courseCategories, _id } = deleteCategories;
    return (
        <div>

            <input type="checkbox" id="categoriesDelete" className="modal-toggle" />
            <div className="modal modal-bottom sm:modal-middle">
                <div className="modal-box">
                    <h3 className="font-bold text-lg text-red-600">Are you sure you want to delete <span className='text-red-500'>{courseCategories}</span> </h3>

                    <div className="modal-action">
                        <label onClick={() => handleDelete(_id)} className='btn btn-xs btn-error'>Yes</label>
                        <label htmlFor="categoriesDelete" className="btn btn-xs">NO</label>
                    </div>
                </div>
            </div>
        </div >
    );
};

export default CategoriesDelete;