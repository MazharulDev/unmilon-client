import React from 'react';
import { toast } from 'react-toastify';

const ContentDelete = ({ deleteContent, setDeleteContent, refetch }) => {
    const handleDelete = id => {
        const url = `${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/content-delete/${id}`
        fetch(url, {
            method: 'DELETE',
            // headers: {
            //     'authorization': `Bearer ${localStorage.getItem('accessToken')}`
            // }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === "success") {
                    toast.success('Content delete successfully')
                    setDeleteContent(null)
                    refetch()
                }

            })
    }

    const { _id, videoName } = deleteContent;
    return (
        <div>

            <input type="checkbox" id="contentDelete" className="modal-toggle" />
            <div className="modal modal-bottom sm:modal-middle">
                <div className="modal-box">
                    <h3 className="font-bold text-lg text-red-600">Are you sure you want to delete <span className='font-extrabold'>{videoName}</span></h3>

                    <div className="modal-action">
                        <label onClick={() => handleDelete(_id)} className='btn btn-xs btn-error'>Yes</label>
                        <label htmlFor="contentDelete" className="btn btn-xs">NO</label>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default ContentDelete;