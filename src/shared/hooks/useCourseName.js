import { useEffect, useState } from "react";

const useCourseName = () => {
    const [names, setNames] = useState([]);
    useEffect(() => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/courses`)
            .then(res => res.json())
            .then(data => setNames(data))
    }, [])
    return [names, setNames];
}
export default useCourseName;