import { useEffect, useState } from "react";

const useGetSubject = () => {
    const [subjects, setSubjects] = useState([]);
    useEffect(() => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/subject`)
            .then(res => res.json())
            .then(data => setSubjects(data))
    }, [])
    return [subjects, setSubjects];
}
export default useGetSubject;