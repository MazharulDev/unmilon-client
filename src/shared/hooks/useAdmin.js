import { useEffect, useState } from "react"

const useAdmin = user => {
    const [admin, setAdmin] = useState(false);
    const [adminLoading, setAdminLoading] = useState(true)
    useEffect(() => {
        const email = user?.email;
        if (email) {
            fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/users/admin/${email}`, {
                method: 'GET',
                headers: {
                    'content-type': 'application/json',
                }
            })
                .then(res => res.json())
                .then(result => {
                    setAdmin(result.data.admin)
                    setAdminLoading(false);
                })
        }
    }, [user])
    return [admin, adminLoading]
}
export default useAdmin;