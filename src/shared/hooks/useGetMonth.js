import { useEffect, useState } from "react";

const useGetMonth = () => {
    const [months, setMonths] = useState([]);
    useEffect(() => {
        fetch(`${process.env.REACT_APP_DATABASE_URL}/api/v1/course-content/month`)
            .then(res => res.json())
            .then(data => setMonths(data))
    }, [])
    return [months, setMonths];
}
export default useGetMonth;