import { Route, Routes, useLocation } from 'react-router-dom';
import './App.css';
import Footer from './components/footer/Footer';
import Header from './components/header/Header';
import Home from './components/home/Home';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Courses from './components/courses/Courses';
import Login from './components/auth/Login';
import SignUp from './components/auth/SignUp';
import ForgotPassword from './components/auth/ForgotPassword';
import CourseDetails from './components/courses/courseDetails/CourseDetails';
import Checkout from './components/courses/checkout/Checkout';
import Dashboard from './components/dashboard/Dashboard';
import CourseList from './components/dashboard/CourseList';
import DashboardHome from './components/dashboard/dashboardHome/DashboardHome';
import RequireAdmin from './shared/RequireAdmin';
import UserProfile from './components/auth/UserProfile';
import UserManagment from './components/dashboard/UserManagment';
import CourseAdd from './components/dashboard/courseAdd/CourseAdd';
import CourseCategoriesAdd from './components/dashboard/courseAdd/CourseCategoriesAdd';
import CourseCategoriesUpdate from './components/dashboard/courseAdd/CourseCategoriesUpdate';
import CategoriesAllCourses from './components/home/CategoriesAllCourses';
import CourseContentShow from './components/courses/courseContentShow/CourseContentShow';
import EditUserProfile from './components/auth/EditUserProfile';
import ShowUserProfile from './components/auth/ShowUserProfile';
import PaymentSuccess from './components/courses/checkout/PaymentSuccess';
import PaymentFailed from './components/courses/checkout/PaymentFailed';
import MyCourses from './components/courses/myCourses/MyCourses';
import CourseContentFields from './components/dashboard/courseAdd/CourseContentFields';
import ContentMonthAdd from './components/dashboard/courseAdd/ContentMonthAdd';
import ContentMonthUpdate from './components/dashboard/courseAdd/ContentMonthUpdate';
import SubjectAdd from './components/dashboard/courseAdd/SubjectAdd';
import SubjectUpdate from './components/dashboard/courseAdd/SubjectUpdate';

function App() {
  const { pathname } = useLocation()
  return (
    <div>
      {pathname?.includes('/dashboard') ? null : <Header />}

      <Routes>
        <Route path='/' element={<Home />} />
        <Route path="/courses" element={<Courses />} />
        <Route path='/login' element={<Login />} />
        <Route path='/signup' element={<SignUp />} />
        <Route path='/profile' element={<UserProfile />}>
          <Route index element={<ShowUserProfile />} />
          <Route path='edit' element={<EditUserProfile />} />
        </Route>
        <Route path='/forgot-password' element={<ForgotPassword />} />
        <Route path='/course-details/:id' element={<CourseDetails />} />
        <Route path='/specificCategories/:categories' element={<CategoriesAllCourses />} />
        <Route path='/my-courses' element={<MyCourses />} />
        <Route path='/checkout/:id' element={<Checkout />} />
        <Route path='/payment/success' element={<PaymentSuccess />} />
        <Route path='/payment/fail' element={<PaymentFailed />} />
        <Route path='/content-show/:id' element={<CourseContentShow />} />
        <Route path='/dashboard' element={<RequireAdmin><Dashboard /></RequireAdmin>}>
          <Route index element={<DashboardHome />} />
          <Route path='courselist' element={<CourseList />} />
          <Route path='courseadd' element={<CourseAdd />} />
          <Route path='contents' element={<CourseContentFields />} />
          <Route path='categoriesadd' element={<CourseCategoriesAdd />} />
          <Route path='month-add' element={<ContentMonthAdd />} />
          <Route path='categoriesupdate/:id' element={<CourseCategoriesUpdate />} />
          <Route path='update-month/:id' element={<ContentMonthUpdate />} />
          <Route path='user-managment' element={<UserManagment />} />

          <Route path="subject-add" element={<SubjectAdd />} />
          <Route path='update-subject/:id' element={<SubjectUpdate />} />
        </Route>
      </Routes>
      {pathname?.includes('/dashboard') ? null : <Footer />}

    </div>
  );
}

export default App;
